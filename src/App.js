import Router from "./routes";
import ThemeConfig from "./theme";
import ScrollToTop from "./helpers/ScrollToTop";

export default function App() {
    return (
        <ThemeConfig>
            <ScrollToTop />            
            <Router />
        </ThemeConfig>
    );
}
