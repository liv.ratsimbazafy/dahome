import { Outlet } from "react-router-dom";
import TopBar from "../../components/client/navigation/TopBar";


export default function ClientLayout() {
    return (
        <>     
            <TopBar />       
            <Outlet />
        </>
    );
}
