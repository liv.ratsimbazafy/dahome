export const FETCH_ALL_PROPERTIES = "FETCH_ALL_PROPERTIES";
export const SET_MAIN_FILTERS = "SET_MAIN_FILTERS";
export const RESOLVE_FILTERS = "RESOLVE_FILTERS";
export const UPDATE_FILTER = "UPDATE_FILTER";
export const SET_SELECTED_FILTERS = "SET_SELECTED_FILTERS";
export const REMOVE_SELECTED_FILTERS = "REMOVE_SELECTED_FILTERS";

export const SET_ALL_PROPERTIES = "SET_ALL_PROPERTIES";
export const CITY_FILTER = "CITY_FILTER";
export const SET_FILTERS = "SET_FILTERS";
export const CALL_FILTERS = "CALL_FILTERS";
export const SET_FILTERED_PROPERTIES = "SET_FILTERED_PROPERTIES";
export const UPDATE_GEOMETRY = "UPDATE_GEOMETRY";
export const REMOVE_FILTER = "REMOVE_FILTER";
export const RESET_FILTER = "RESET_FILTER";
