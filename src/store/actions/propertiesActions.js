import {
    FETCH_ALL_PROPERTIES,
    SET_MAIN_FILTERS,
    UPDATE_FILTER,
    SET_SELECTED_FILTERS,
    REMOVE_SELECTED_FILTERS,
    CITY_FILTER,
    CALL_FILTERS,
    SET_FILTERED_PROPERTIES,
    UPDATE_GEOMETRY,
    REMOVE_FILTER,
    RESET_FILTER,
} from "../constants/propertiesConstants";

export function fetchAllProperties(properties) {
    return {
        type: FETCH_ALL_PROPERTIES,
        payload: properties,
    };
}

export function setMainFilters(filters) {
    return {
        type: SET_MAIN_FILTERS,
        payload: filters,
    };
}

export function updateFilter(payload) {
    return {
        type: UPDATE_FILTER,
        payload: payload,
    };
}

export function setSelectedFilters(payload) {
    return {
        type: SET_SELECTED_FILTERS,
        payload: payload,
    };
}

export function removeSelectedFilter(payload) {
    return {
        type: REMOVE_SELECTED_FILTERS,
        payload: payload,
    };
}

//================== @to do =====================

export function cityFilter(city) {
    return {
        type: CITY_FILTER,
        payload: city,
    };
}

export function callFilters() {
    return {
        type: CALL_FILTERS,
        payload: null,
    };
}

export function setFilteredProperties(properties) {
    return {
        type: SET_FILTERED_PROPERTIES,
        payload: properties,
    };
}

export function updateGeometry(newGeometry) {
    return {
        type: UPDATE_GEOMETRY,
        payload: newGeometry,
    };
}

export function removeFilter(filter) {
    return {
        type: REMOVE_FILTER,
        payload: filter,
    };
}

export function resetFilter() {
    return {
        type: RESET_FILTER,
        payload: {},
    };
}
