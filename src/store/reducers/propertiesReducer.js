import { filterData } from "../../helpers/filters";
import {
    FETCH_ALL_PROPERTIES,
    SET_MAIN_FILTERS,
    UPDATE_FILTER,
    SET_SELECTED_FILTERS,
    REMOVE_SELECTED_FILTERS,
    CITY_FILTER,
    SET_ALL_PROPERTIES,
    CALL_FILTERS,
    SET_FILTERED_PROPERTIES,
    UPDATE_GEOMETRY,
    REMOVE_FILTER,
    RESET_FILTER,
} from "../constants/propertiesConstants";

const initialState = {
    allProperties: [],
    filteredProperties: [],
    filters: {},
    selectedFilters: [],
    additionalFilters: {},
};
export default function propertiesReducer(
    state = initialState,
    { type, payload }
) {
    switch (type) {
        case FETCH_ALL_PROPERTIES:
            return {
                ...state,
                allProperties: [...payload],
            };
        case SET_MAIN_FILTERS:
            return {
                ...state,
                customFilters: { ...payload.filters },
                geometry: { ...payload.geometry },
            };
        case CALL_FILTERS:
            return {
                ...state,
                filteredProperties: [
                    ...filterData(state.allProperties, state.customFilters),
                ],
            };
        case SET_SELECTED_FILTERS:
            let itemToAdd = [];
            if (!state.selectedFilters.includes(payload))
                itemToAdd.push(payload);
            return {
                ...state,
                selectedFilters: [...state.selectedFilters, ...itemToAdd],
            };
        case REMOVE_SELECTED_FILTERS:
            const actualFilters = state.selectedFilters.filter(
                (filter) => filter !== payload
            );
            return {
                ...state,
                selectedFilters: [...actualFilters],
            };
        case UPDATE_FILTER:
            let newFilter;
            if (payload.type === "nested")
                newFilter = {
                    ...state.customFilters,
                    properties: {
                        ...state.customFilters.properties,
                        ...payload.value,
                    },
                };
            else newFilter = { ...state.customFilters, ...payload };
            return {
                ...state,
                customFilters: newFilter,
            };

        case CITY_FILTER:
            return {
                ...state,
                customFilters: {
                    ...state.customFilters,
                    properties: {
                        ...state.customFilters.properties,
                        city: payload,
                    },
                },
            };

        case SET_ALL_PROPERTIES:
            return {
                ...state,
                allProperties: [...payload],
            };
        case SET_FILTERED_PROPERTIES:
            return {
                ...state,
                filteredProperties: [...payload],
            };
        case UPDATE_GEOMETRY:
            return {
                ...state,
                geometry: {
                    ...state.geometry,
                    lat: payload.lat,
                    lng: payload.lng,
                },
            };
        case REMOVE_FILTER:
            //@todo check if some filters remain in customFilter state => if filters && apply them
            const newState = Object.assign({}, state.customFilters);
            if (payload.length > 1) delete newState[payload[0]][payload[1]];
            else delete newState[payload[0]];
            return {
                ...state,
                additionalFilters: { ...newState },
            };
        case RESET_FILTER:
            return {
                ...state,
                additionalFilters: {},
            };
        default:
            return state;
    }
}
