import {combineReducers} from "redux";
import asyncReducer from "./reducers/asyncReducer";
import propertiesReducer from "./reducers/propertiesReducer";

const rootReducer = combineReducers({
    async: asyncReducer,
    dahome: propertiesReducer,
});

export default rootReducer;
