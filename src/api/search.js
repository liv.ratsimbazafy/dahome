import axios from "axios";

export const deptsAutocomplete = async (query) => {
    return axios.get(`https://api-adresse.data.gouv.fr/search/?q=${query}&type=municipality&autocomplete=1`)
}
