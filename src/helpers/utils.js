/**
 * Allow to display loading indicator and skeleton placeholder while fetching data
 * @param {int} ms 
 * @returns 
 */

 export function delay(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
