export function formatCity(city) {   
    return city && city
        .substring(8)
        .replace(/-/g, " ")
        .replace(/'/g, " ")  
        .normalize("NFD")
        .replace(/[\u0300-\u036f]/g, "")     
        .toUpperCase();
}
