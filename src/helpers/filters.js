export function filterObj(array, filters) {
    return array.filter((item) => {
        for (const [key, value] of Object.entries(filters)) {
            if (item[key] === undefined || item[key] != filters[key] || typeof item[key] === "object") {
                for (let objVal in value) {
                    if (String(item[key][objVal]).toUpperCase() != String(value[objVal]).toUpperCase()) return false;
                }
            }
        }
        return true;
    });
}

export function equalFilter(state, objProp, filterVal) {
    let sanitize;

    if (objProp === "price") {
        sanitize = (item) => parseInt(item.replace(/\s/g, ""));
    } else if (objProp === "surface") {
        sanitize = (item) => parseInt(item.replace(/\s/g, "").replace("m2", ""));
    }

    return state.filter((property) => sanitize(property.properties[`${objProp}`]) >= parseInt(filterVal.min) && sanitize(property.properties[`${objProp}`]) <= parseInt(filterVal.max));
}

export function mapDropdownToState(dropdownArray) {
    let accumulator = [];
    dropdownArray.forEach((item, i) => {
        if (dropdownArray[i][Object.keys(item)]) accumulator.push(Object.keys(item)[0]);
    });
    switch (accumulator.length) {
        case 0:
            return {type: (type) => type === "maison"};
        case 1:
            return {type: (type) => type === accumulator[0]};
        case 2:
            return {
                type: (type) => type === accumulator[0] || type === accumulator[1],
            };
        case 3:
            return {
                type: (type) => type === accumulator[0] || type === accumulator[1] || type === accumulator[2],
            };
        default:
            return {type: (type) => type === "maison"};
    }
}

export function resolveAllFilters(state, filters) {
    let tempData = [];
    tempData = filterByProps(state, filters);
    if (tempData.length !== 0) return resolveNestedProp(tempData, filters); else return tempData;
}

export function filterByProps(array, filters) {
    const filterKeys = Object.keys(filters);
    return array.filter((item) => {
        // validates all filter criteria
        return filterKeys.every((key) => {
            // ignores non-function predicates
            if (typeof filters[key] !== "function") return true;
            return filters[key](item[key]);
        });
    });
}

function resolveNestedProp(array, filters) {
    return array.filter((element, i) => {
        for (const property in filters) {
            if (typeof filters[property] === "object") {
                const nestedProps = Object.keys(filters[property]);
                return nestedProps.every((value) => {
                    return filters[property][value](element[property][value]);
                });
            }
        }
    });
}

// ==================================================================
/**
 *Map main object filter properties as function properties
 *
 * @param {Array} propertyType
 * @returns {object}
 */
function mapPropertyTypeToFilter(propertyType) {
    switch (propertyType.length) {
        case 0:
            return {type: (type) => type === "2"}; // default Maison
        case 1:
            return {type: (type) => type === String(propertyType[0])};
        case 2:
            return {
                type: (type) => type === String(propertyType[0]) || String(type === propertyType[1]),
            };
        case 3:
            return {
                type: (type) => type === String(propertyType[0]) || String(type === propertyType[1]) || String(type === propertyType[2]),
            };

        default:
            return {type: (type) => type === "2"}; // default Maison
    }
}

/**
 * Map nested object filter properties as function properties
 *
 * @param {Object} obj
 * @param {String} objProperty
 * @param {Number} objValue
 * @returns {Object}
 */
function mapNestedObjectPropsToFilter(obj, objProperty, objValue) {
    switch (String(objProperty)) {
        case "price":
            return (obj["properties"][objProperty] = (objProperty) => parseInt(objProperty.replace(/\s/g, "")) <= objValue);
        case "surface":
            return (obj["properties"][objProperty] = (objProperty) => parseInt(objProperty.replace(/\s/g, "").replace("m2", "")) <= objValue);
        case "room":
            switch (objValue) {
                case 5:
                    return (obj["properties"][objProperty] = (objProperty) => objProperty >= objValue);

                default:
                    return (obj["properties"][objProperty] = (objProperty) => objProperty <= objValue);
            }

        default:
            return (obj["properties"][objProperty] = (objProperty) => objProperty === objValue);
    }
}

/**
 * Build a filter object according to search parameters
 *
 * @param {Object} filters
 * @returns {Object}
 */
function filterBuilder(filters) {
    let mainFilter = {
        target: (target) => target === filters.target, properties: {},
    };
    mainFilter = Object.assign(mainFilter, mapPropertyTypeToFilter(filters.type));

    for (let property in filters) {
        if (typeof filters[property] === "object" && !Array.isArray(filters[property])) {
            for (const [key, value] of Object.entries(filters[property])) {
                mapNestedObjectPropsToFilter(mainFilter, key, value);
            }
        }
    }
    return mainFilter;
}

/**
 * Filter an array of object by the given criteria
 *
 * @param {Array} array
 * @param {Object} filters
 * @returns {Array}
 */
function filterByProperties(array, filters) {
    const filterKeys = Object.keys(filters);
    return array.filter((item) => {
        // validates all filter criteria
        return filterKeys.every((key) => {
            // ignores non-function predicates
            if (typeof filters[key] !== "function") return true;
            return filters[key](item[key]);
        });
    });
}

/**
 * Filter one level deep nested object of a nested array of object by the given criteria
 *
 * @param {Array} array
 * @param {Object} filters
 * @returns {Array}
 */
function filterByNestedProperties(array, filters) {
    return array.filter((element, i) => {
        for (const property in filters) {
            if (typeof filters[property] === "object") {
                const nestedProps = Object.keys(filters[property]);
                return nestedProps.every((value) => {
                    return filters[property][value](element[property][value]);
                });
            }
        }
    });
}

/**
 *
 * @param {Array} data
 * @param {Object} filters
 * @returns {Array}
 */
export function filterData(data, filters) {
    let tmpData;
    const newFilters = filterBuilder(filters);
    tmpData = filterByProperties(data, newFilters);
    return filterByNestedProperties(tmpData, newFilters);
}

export function getSelectedPropertyType(checkboxData) {
    let selectedPropertyTypes = [];
    checkboxData.forEach((checked, i) => {
        if (checked) {
            if (i === 0) selectedPropertyTypes.push("2"); else if (i === 1) selectedPropertyTypes.push("1"); else if (i === 2) selectedPropertyTypes.push("3");
        }
    });
    return selectedPropertyTypes;
}
