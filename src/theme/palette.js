import { common } from "@mui/material/colors";
import { alpha } from "@mui/material/styles";

// ----------------------------------------------------------------------

function createGradient(color1, color2) {
    return `linear-gradient(to bottom, ${color1}, ${color2})`;
}

// SETUP COLORS
const GREY = {
    0: "#FFFFFF",
    100: "#FBFCFD",
    200: "#F8F9FC",
    300: "#F4F7FA",
    400: "#F1F4F9",
    500: "#F1F4F9",
    600: "#ACBEDC",
    700: "#3B5789",
    800: "#1D2C45",
    900: "#182439",
    500_8: alpha("#919EAB", 0.08),
    500_12: alpha("#919EAB", 0.12),
    500_16: alpha("#919EAB", 0.16),
    500_24: alpha("#919EAB", 0.24),
    500_32: alpha("#919EAB", 0.32),
    500_48: alpha("#919EAB", 0.48),
    500_56: alpha("#919EAB", 0.56),
    500_80: alpha("#919EAB", 0.8),
};

const PRIMARY = {
    lighter: "#EDF3FB",
    light: "#CADAF3",
    main: "#0A172A",
    dark: "#0A172A",
    darker: "#000000",
    contrastText: "#EDF3FB",
};
const SECONDARY = {
    lighter: "#FCF0D2",
    light: "#F8DA8F",
    main: "#F0B212",
    dark: "#D29A0D",
    darker: "#BA890C",
    contrastText: "#FFFFFF",
};
const INFO = {
    lighter: "#FFFFFF",
    light: "#FFFFFF",
    main: "#FFFFFF",
    dark: "#FFF",
    darker: "#99D5F7",
    contrastText: GREY[900],
};
const SUCCESS = {
    lighter: "#ACEDED",
    light: "#77D5D5",
    main: "#4EBABA",
    dark: "#499264",
    darker: "#168C8C",
    contrastText: GREY[500],
};
const WARNING = {
    lighter: "#FCF0D2",
    light: "#F8DA8F",
    main: "#F0B212",
    dark: "#D23D29",
    darker: "#BA890C",
    contrastText: "#FFFFFF",
};
const ERROR = {
    lighter: "#FFBABA",
    light: "#FF8F8F",
    main: "#FF6B6B",
    dark: "#FF4C4C",
    darker: "#E92525",
    contrastText: GREY[800],
};

const GRADIENTS = {
    primary: createGradient(PRIMARY.light, PRIMARY.main),
    info: createGradient(INFO.light, INFO.main),
    success: createGradient(SUCCESS.light, SUCCESS.main),
    warning: createGradient(WARNING.light, WARNING.main),
    error: createGradient(ERROR.light, ERROR.main),
};


const palette = {
    common: { black: "#000", white: "#ffffff", bg: "#e1e8e5" },
    primary: { ...PRIMARY },
    secondary: { ...SECONDARY },
    info: { ...INFO },
    success: { ...SUCCESS },
    warning: { ...WARNING },
    error: { ...ERROR },
    grey: GREY,
    gradients: GRADIENTS,    
    divider: GREY[500_24],
    text: { primary: PRIMARY.main, secondary: GREY[800], disabled: GREY[100] },
    background: { paper: GREY[500], default: common.white, neutral: GREY[100] },
    action: {
        active: GREY[600],
        hover: GREY[500_8],
        selected: GREY[500_16],
        disabled: GREY[500_80],
        disabledBackground: GREY[500_24],
        focus: GREY[500_24],
        hoverOpacity: 0.08,
        disabledOpacity: 0.48,
    },
};

export default palette;
