import "simplebar/src/simplebar.css";

import { useRoutes } from "react-router-dom";
import ClientLayout from "./layouts/client";
import LandingPage from "./pages/client/LandingPage";
import PropertyDetail from "./pages/client/PropertyDetail";
import PropertyList from "./pages/client/PropertyList";

export default function Router() {
    return useRoutes([
        {
            path: "/",
            element: <ClientLayout />,
            children: [
                { path: "/", element: <LandingPage /> },               
                { path: "/rent", element: <PropertyList /> },
                { path: "/buy", element: <PropertyList /> },
                { path: "/buy/:id", element: <PropertyDetail /> },
            ],
        },
    ]);
}
