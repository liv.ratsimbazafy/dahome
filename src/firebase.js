/** Fire base SDK v9*/
import firebase from "firebase/compat/app";
import "firebase/compat/firestore";
import "firebase/compat/database";
import "firebase/compat/auth";
import "firebase/compat/storage";
import {getAuth} from "firebase/auth"; // Testing authentification

const firebaseConfig = {
    apiKey: process.env.REACT_APP_FBS_API_KEY,
    authDomain: process.env.REACT_APP_FBS_AUTH_DOMAIN,
    projectId: process.env.REACT_APP_FBS_PROJECT_ID,
    storageBucket: process.env.REACT_APP_FBS_BUCKET,
    messagingSenderId: process.env.REACT_APP_FBS_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_FBS_APP_ID,
    measurementId: "G-8CGCKK2LPX",
};

firebase.initializeApp(firebaseConfig);
firebase.firestore();

export default firebase;

export const auth = getAuth();
