import React, { useState, useEffect, Fragment } from "react";
import { useSearchParams } from "react-router-dom";
import { styled } from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import PageWrapper from "../../components/layout/PageWrapper";
import CardItemSkeletons from "../../components/client/properties/CardItemSkeletons";
import PropertiesMap from "../../components/client/properties/PropertiesMap";
import PropertiesFilters from "../../components/client/properties/filters/PropertiesFilters";
import PropertyCard from "../../components/client/properties/PropertyCard";

import { useDispatch, useSelector } from "react-redux";
import {
    callFilters,
    resetFilter,
} from "../../store/actions/propertiesActions";

const ContainerStyle = styled(Box)(({ theme }) => ({
    display: "flex",
    justifyContent: "flex-start",
    [theme.breakpoints.down("md")]: {
        gap: 0,
    },
    gap: "2rem",
}));

const MapWrapper = styled(Box)(({ theme }) => ({
    [theme.breakpoints.down("md")]: {
        display: "none",
    },
    [theme.breakpoints.down("xl")]: {
        paddingRight: "2rem",
    },
    width: "50%",
    maxWidth: 600,
    height: "70vh",
    position: "fixed",
}));

const MapWrapperFullScreen = styled(Box)(({ theme }) => ({
    width: "100%",
    height: "70vh",
}));

const FiltersWrapper = styled(Box)(({ theme }) => ({
    position: "absolute",
    top: "5rem",
    width: "100%",
    maxWidth: "1488px",
    zIndex: 100,
}));

const NoPropertyFound = styled(Box)(({ theme }) => ({
    minHeight: 300,
    margin: "5rem auto",
    maxWidth: 600,
    background: theme.palette.text.disabled,
    borderRadius: 10,
    padding: 30,
    display: "flex",
    justifyContent: "center",
    alignItems: "flex-start",
}));

export default function PropertyList() {
    const { filteredProperties, customFilters, geometry } = useSelector(
        (state) => state.dahome
    );
    const dispatch = useDispatch();
    const [searchTerm, setSearchTerm] = useState({});
    const [searchParams] = useSearchParams();
    const [selectedCity, setSelectedCity] = useState(null);
    const [loading, setLoading] = useState(true);
    const [markerColor, setMarkerColor] = useState(null);
    const [isImageLoaded, setIsImageLoaded] = useState(false);
    const [isMix, setIsMix] = useState(true);

    useEffect(() => {
        const queryParams = Object.fromEntries([...searchParams]);
        setSearchTerm(queryParams);
        setLoading(false);
    }, [searchParams, geometry]);

    const getSelectedItem = (item, e) => {
        e.preventDefault();
        e.stopPropagation();
        setSelectedCity(item);
        setMarkerColor(item.id);
    };

    const onMouseOver = (e, cityId) => {
        e.preventDefault();
        setMarkerColor(cityId);
    };

    const onMouseOut = () => {
        setMarkerColor(null);
    };

    // const resetAllFilters = () => {
    //     dispatch(resetFilter());
    //     dispatch(callFilters({}));
    // };

    const NotFound = () =>
        filteredProperties.length < 1 && (
            <NoPropertyFound>
                <Box>
                    <Typography
                        variant="h4"
                        marginBottom={4}
                        textTransform={"uppercase"}
                        color="primary"
                    >
                        Aucun bien trouvé
                    </Typography>
                    <Typography variant="body1">
                        DÉSOLÉ NOUS N'AVONS PAS DE BIEN À VOUS PROPOSER AVEC CES
                        CRITÈRES DE RECHERCHE POUR LE MOMENT.
                    </Typography>
                </Box>
            </NoPropertyFound>
        );

    const handleImageLoad = () => {
        setIsImageLoaded(true);
    };

    const handleMainTabsChange = () => {
        setIsMix(!isMix);
    };

    return (
        <PageWrapper title="Recherche">
            <Container maxWidth="xl" sx={{ mt: { xs: 20, md: 25 } }}>
                <FiltersWrapper>
                    <Box
                        sx={{
                            position: "fixed",
                            width: "100%",
                            maxWidth: "1490px",
                            minHeight: 100,
                            py: 3,
                        }}
                    >
                        <NotFound />
                        <PropertiesFilters
                            setLoading={setLoading}
                            isMix={isMix}
                            handleMainTabsChange={handleMainTabsChange}
                        />
                    </Box>
                </FiltersWrapper>

                <ContainerStyle>
                    {isMix ? (
                        <Fragment>
                            {/* List */}
                            <Box
                                component={"div"}
                                sx={{
                                    width: {
                                        xs: "100%",
                                        sm: "100%",
                                        md: "60%",
                                    },
                                }}
                            >
                                <Grid
                                    container
                                    rowSpacing={{ xs: 5, sm: 4, md: 4 }}
                                    columnSpacing={{ sm: 4, md: 4 }}
                                >
                                    {loading && !isImageLoaded && (
                                        <CardItemSkeletons cardNum={4} />
                                    )}

                                    {filteredProperties.map((property, i) => {
                                        return (
                                            <Grid
                                                item
                                                key={i}
                                                xs={12}
                                                sm={6}
                                                md={6}
                                            >
                                                <PropertyCard
                                                    property={property}
                                                    onMouseOver={onMouseOver}
                                                    onMouseOut={onMouseOut}
                                                    handleImageLoad={
                                                        handleImageLoad
                                                    }
                                                />
                                            </Grid>
                                        );
                                    })}
                                </Grid>
                            </Box>
                            {/* Map */}
                            <Box
                                sx={{
                                    width: { xs: "0%", sm: "0%", md: "40%" },
                                    height: "100%",
                                    position: "relative",
                                    display: { sm: "none", md: "block" },
                                }}
                            >
                                {loading ? (
                                    "..."
                                ) : (
                                    <MapWrapper>
                                        <PropertiesMap
                                            properties={filteredProperties}
                                            getSelectedItem={getSelectedItem}
                                            setSelectedCity={setSelectedCity}
                                            selectedCity={selectedCity}
                                            setMarkerColor={setMarkerColor}
                                            markerColor={markerColor}
                                        />
                                    </MapWrapper>
                                )}
                            </Box>
                        </Fragment>
                    ) : loading ? (
                        "..."
                    ) : (
                        <MapWrapperFullScreen>
                            <PropertiesMap
                                properties={filteredProperties}
                                getSelectedItem={getSelectedItem}
                                setSelectedCity={setSelectedCity}
                                selectedCity={selectedCity}
                                setMarkerColor={setMarkerColor}
                                markerColor={markerColor}
                            />
                        </MapWrapperFullScreen>
                    )}
                </ContainerStyle>
            </Container>
        </PageWrapper>
    );
}
