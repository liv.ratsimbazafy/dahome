import React, { useEffect, useState } from "react";
import axios from "axios";
import { useDispatch } from "react-redux";
import { styled } from "@mui/material/styles";
import PageWrapper from "../../components/layout/PageWrapper";
import SearchWidget from "../../components/client/properties/filters/SearchWidget";
import landingImg from "../../assets/images/furniture.jpg";
import {
    Typography,
    Box,
    Container,
    Grid,
    Card,
    CardContent,
    CardMedia,
} from "@mui/material";
import Heading from "../../components/client/Heading";
import PropertyCard from "../../components/client/properties/PropertyCard";
import {
    fetchAllProperties,
    setFilteredProperties,
} from "../../store/actions/propertiesActions";

const Hero = styled("section")(({ theme }) => ({
    width: "100%",
    height: "100vh",
    minHeight: 870,
    position: "relative",
    backgroundColor: "#f1f1f1",
    backgroundImage: `url(${landingImg})`,
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
}));

const headerStyle = {
    position: "absolute",
    top: { xs: "30%", md: "35%" },
    left: "50%",
    transform: "translate(-50%, -50%)",
    textAlign: "center",
    width: "100%",
};

const lastProperties = [
    {
        id: 1,
        title: "Maison 7 pièces 172",
        price: "850.000",
        description: "Olonne sur Mer",
        thumbnails: [
            "https://images.unsplash.com/photo-1570129477492-45c003edd2be?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80",
        ],
        properties: {
            typeBien: "maison",
            city: "OLONNE SUR MER",
            room: 3,
            surface: "127 m2",
        },
    },
    {
        id: 2,
        title: "Appartement 3 pièces 70",
        price: "340.000",
        description: "Les Sables D'Olonne",
        thumbnails: [
            "https://images.unsplash.com/photo-1501183638710-841dd1904471?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80",
        ],
        properties: {
            typeBien: "Appartement",
            city: "OLONNE SUR MER",
            room: 3,
            surface: "89 m2",
        },
    },
    {
        id: 3,
        title: "Maison 4 pièces 112",
        price: "580.000",
        description: "Saint Mathurin",
        thumbnails: [
            "https://images.unsplash.com/photo-1523217582562-09d0def993a6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
        ],
        properties: {
            typeBien: "maison",
            city: "OLONNE SUR MER",
            room: 3,
            surface: "95 m2",
        },
    },
];

const howTosellItems = [
    {
        title: "Nous évaluons votre maison et nous vous aidons à la vendre.",
        img: "https://images.unsplash.com/photo-1554415707-c1426270e0da?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80",
    },
    {
        title: "Trouvez le meilleur prix à votre convenance.",
        img: "https://images.unsplash.com/photo-1551836022-d5d88e9218df?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80",
    },
    {
        title: "Nous prenons soin de la vente du début jusqu'à la fin.",
        img: "https://images.unsplash.com/photo-1549923746-c502d488b3ea?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80",
    },
];

export default function LandingPage() {
    const dispatch = useDispatch();
    const [allData, setAllData] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            let citiesUrl = "../../data/city.json";
            const api = axios.create({
                withCredentials: true,
                baseUrl: citiesUrl,
            });
            const { data } = await api.get(citiesUrl);
            setAllData(data);
            dispatch(fetchAllProperties(data));
        };

        fetchData();
        dispatch(setFilteredProperties([]));
    }, []);

    return (
        <PageWrapper title="home">
            <Hero>
                <Box sx={headerStyle}>
                    <Typography
                        variant="h1"
                        sx={{
                            fontWeight: 700,
                            letterSpacing: "-1.5px",
                        }}
                    >
                        Un projet immobilier ?
                    </Typography>
                    <Typography
                        variant="body1"
                        sx={{
                            fontSize: { xs: 20, md: 39 },
                            fontWeight: 700,
                            color: "primary.main",
                            mt: 1,
                        }}
                    >
                        Réalisons-le ensemble.
                    </Typography>
                </Box>
                <SearchWidget allData={allData} />
            </Hero>

            <Container maxWidth={"xl"}>
                <Grid
                    container
                    justifyContent={{
                        xs: "space-between",
                        sm: "center",
                        md: "space-between",
                    }}
                    alignItems="center"
                    sx={{ py: { xs: 10, md: 20 } }}
                    columnSpacing={{ xs: 5, sm: 3, md: 0 }}
                    rowSpacing={{ xs: 5, sm: 0, md: 0 }}
                >
                    <Grid item xs={12} sm={12} md={4}>
                        <Heading
                            subHeader="DAHOME"
                            header="L'immobilier à votre service."
                        />
                        <Typography
                            variant="body1"
                            sx={{ mt: 5, px: { xs: 0, sm: 8, md: 0 } }}
                        >
                            <strong>DAHOME</strong> est une agence indépendante
                            proche de ses clients. Une équipe
                            expérimentée&nbsp;pour mener efficacement votre
                            projet.&nbsp; Nous sommes à votre entière
                            disposition pour vous apporter tous conseils en
                            vente, achat, estimation, financement … et vous
                            accompagner dans tous vos projets.
                        </Typography>
                    </Grid>
                    <Grid item xs={12} sm={10} md={6}>
                        <Box
                            component="img"
                            sx={{
                                height: "auto",
                                width: "100%",
                                objectFit: "cover",
                                borderRadius: 2,
                            }}
                            alt="The house from the offer."
                            src="https://images.unsplash.com/photo-1559329145-afaf18e3f349?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80"
                        />
                    </Grid>
                </Grid>

                {/* section latest properties */}

                <Heading
                    subHeader="dernières annonces"
                    header=" Découvrer nos derniers biens en vente."
                />

                <Grid
                    container
                    justifyContent={{ sm: "center", md: "space-between" }}
                    alignItems={"center"}
                    columnSpacing={5}
                    rowSpacing={5}
                    paddingTop={10}
                >
                    {lastProperties.map((property, i) => {
                        return (
                            <Grid item key={i} xs={12} sm={10} md={4}>
                                <PropertyCard property={property} />
                            </Grid>
                        );
                    })}
                </Grid>

                {/* section how to sell */}
                <Box
                    component={"div"}
                    sx={{
                        py: 10,
                        mt: 20,
                        mb: 10,
                        bgcolor: { xs: "transparent", sm: "grey.500" },
                        borderRadius: 3,
                    }}
                >
                    <Typography
                        variant="h2"
                        textAlign={"center"}
                        sx={{
                            margin: "auto",
                            maxWidth: 700,
                            mb: 10,
                        }}
                    >
                        Vender votre maison en toute simplicité.
                    </Typography>
                    <Grid
                        container
                        justifyContent={{
                            sm: "center",
                            md: "space-evently",
                            lg: "center",
                        }}
                        alignItems={"center"}
                        columnSpacing={5}
                        rowSpacing={5}
                    >
                        {howTosellItems.map((item, i) => {
                            return (
                                <Grid
                                    item
                                    key={i}
                                    xs={12}
                                    sm={10}
                                    md={3}
                                    lg={3}
                                >
                                    <Card
                                        sx={{
                                            border: "none",
                                            bgcolor: {
                                                xs: "grey.500",
                                                sm: "common.white",
                                            },
                                        }}
                                    >
                                        <CardContent
                                            sx={{ p: 3, minHeight: 110 }}
                                        >
                                            <Typography variant="h6">
                                                {item.title}
                                            </Typography>
                                        </CardContent>
                                        <CardMedia
                                            component="img"
                                            image={item.img}
                                            width="100%"
                                            height="200"
                                            sx={{ borderRadius: 2 }}
                                        />
                                    </Card>
                                </Grid>
                            );
                        })}
                    </Grid>
                </Box>
            </Container>
        </PageWrapper>
    );
}
