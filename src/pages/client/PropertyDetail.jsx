import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import CardMedia from "@mui/material/CardMedia";
import Chip from "@mui/material/Chip";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import Stack from "@mui/material/Stack";
import PageWrapper from "../../components/layout/PageWrapper";
import { useParams } from "react-router-dom";
import CameraAltIcon from "@mui/icons-material/CameraAlt";
import Carousel from "react-material-ui-carousel";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import UploadIcon from "@mui/icons-material/Upload";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import PropertyAgencyCard from "../../components/client/properties/PropertyAgencyCard";
import { ReactComponent as ArchiIcon } from "../../assets/svg/architecture.svg";
import { ReactComponent as PlanIcon } from "../../assets/svg/plan.svg";
import { ReactComponent as BathIcon } from "../../assets/svg/bathtub.svg";
import { ReactComponent as BedroomIcon } from "../../assets/svg/bedroom.svg";
import { ReactComponent as EstateIcon } from "../../assets/svg/estate.svg";
import PropertyCharacteristics from "../../components/client/properties/PropertyCharacteristics";
import PropertyEnergy from "../../components/client/properties/PropertyEnergy";
import PropertyDetailsModal from "../../components/client/properties/PropertyDetailsModal";

export default function PropertyDetail() {
    const { id } = useParams();
    const [property, setProperty] = useState("");
    const [isLoading, setIsLoading] = useState(true);
    const navigate = useNavigate();
    const [openPropDetailsModal, setOpenPropDetailsModal] = useState(false);

    const handleOpenPropDetailsModal = () => setOpenPropDetailsModal(true);
    const handleClosePropDetailsModal = () => setOpenPropDetailsModal(false);

    useEffect(() => {
        async function fetchData() {
            let citiesUrl = "../../data/city.json";
            const data = await fetch(citiesUrl);
            const json = await data.json();
            const rentProperties = json.find((item) => {
                return item.id == id;
            });
            setProperty(rentProperties);
            setIsLoading(false);
        }

        fetchData();

        navigator.geolocation.getCurrentPosition((success, error) => {
            console.log(success);
        });
    }, []);

    const RenderCarousel = () => (
        <Carousel
            autoPlay={false}
            indicators={true}
            navButtonsProps={{
                style: {
                    backgroundColor: "#ffffff",
                    color: "#0A172A",
                    opacity: 1,
                },
            }}
            indicatorIconButtonProps={{
                style: {
                    color: "#fff",
                    padding: "5px",
                    zIndex: 999,
                },
            }}
            activeIndicatorIconButtonProps={{
                style: {
                    color: "#F0B212",
                    zIndex: 999,
                },
            }}
            indicatorContainerProps={{
                style: {
                    position: "absolute",
                    bottom: "1.5rem",
                },
            }}
            sx={{ borderRadius: { xs: 0, md: 2 } }}
        >
            {property.thumbnails.map((photo, index) => {
                return (
                    <CardMedia
                        key={index}
                        component="img"
                        //height="700px"
                        image={photo}
                        alt={"property-" + index}
                        sx={{ height: { xs: 400, md: 600 } }}
                    />
                );
            })}
        </Carousel>
    );

    if (isLoading) {
        return (
            <PageWrapper>
                <Container maxWidth="xl" sx={{ mt: 15 }}>
                    <Typography>...loading</Typography>
                </Container>
            </PageWrapper>
        );
    }

    const PropertySummary = () => {
        return (
            <>
                <Stack
                    direction={{ xs: "column", md: "row" }}
                    marginTop={5}
                    justifyContent={"space-between"}
                >
                    <Stack direction={"row"} spacing={1}>
                        <Typography variant="h5" textTransform={"capitalize"}>
                            {property.name.toLowerCase()}
                        </Typography>
                    </Stack>
                </Stack>
                <Stack
                    direction={"row"}
                    alignItems="center"
                    marginTop={1}
                    spacing={2}
                >
                    <Typography variant="h4">{property.price} €</Typography>
                    <Chip
                        size="small"
                        label={
                            "dont " +
                            property.properties.honoraires +
                            " d'honoraires."
                        }
                    />
                </Stack>
                <Divider sx={{ my: 5 }} />
                <Box>
                    <Typography variant="h4" marginBottom={3}>
                        Avis du professionnel.
                    </Typography>
                    <Typography variant="body1">
                        {property.description}
                    </Typography>
                </Box>
            </>
        );
    };

    const NavLinkWrapper = () => {
        return (
            <Container
                maxWidth="lg"
                sx={{ mt: { xs: 9, md: 15 }, mb: { xs: 1.5, md: 3 } }}
            >
                <Stack
                    direction={"row"}
                    justifyContent={"space-between"}
                    alignItems={"center"}
                >
                    <Box>
                        <IconButton
                            onClick={() => navigate(-1)}
                            color="primary"
                        >
                            <KeyboardArrowLeftIcon />
                        </IconButton>
                    </Box>
                    <Stack
                        direction={"row"}
                        justifyContent={"space-between"}
                        alignItems={"center"}
                        spacing={{ xs: 2, sm: 2, md: 2 }}
                    >
                        <Button
                            size="small"
                            variant="text"
                            startIcon={<UploadIcon />}
                        >
                            {/* {<Hidden smDown>Partager</Hidden>} */}
                            Partager
                        </Button>

                        <Button
                            size="small"
                            variant="text"
                            startIcon={<FavoriteBorderIcon />}
                        >
                            {/* {<Hidden smDown>Enregistrer</Hidden>} */}
                            Enregistrer
                        </Button>
                    </Stack>
                </Stack>
            </Container>
        );
    };

    const PropertyImagesWrapper = () => {
        return (
            <Container maxWidth="lg" sx={{ px: { xs: 0, md: 2 } }}>
                <Grid
                    container
                    flexDirection={{ sm: "column", md: "row" }}
                    columnSpacing={1}
                >
                    <Grid item xs={12} sm={12} md={8}>
                        <Box
                            sx={{
                                position: "relative",
                                width: "100%",
                                height: { xs: 350, sm: 502 },
                            }}
                        >
                            <Box
                                component="img"
                                src={property.thumbnails[0]}
                                sx={{
                                    objectFit: "cover",
                                    width: "100%",
                                    height: "100%",
                                    borderTopLeftRadius: { xs: 0, md: 15 },
                                    borderBottomLeftRadius: { xs: 0, md: 15 },
                                }}
                            />
                            <Button
                                onClick={handleOpenPropDetailsModal}
                                size="small"
                                startIcon={<CameraAltIcon />}
                                variant="contained"
                                color="info"
                                sx={{
                                    position: "absolute",
                                    bottom: 24,
                                    right: 24,
                                    display: { xs: "flex", md: "none" },
                                }}
                            >
                                {"Afficher " +
                                    property.thumbnails.length +
                                    " photos"}
                            </Button>
                        </Box>
                    </Grid>

                    <Grid
                        container
                        item
                        md={4}
                        sx={{ display: { xs: "none", md: "flex" } }}
                    >
                        <Grid item md={12} rowSpacing={1}>
                            <Box
                                component="img"
                                src={property.thumbnails[1]}
                                sx={{
                                    width: "100%",
                                    borderTopRightRadius: 15,
                                    height: 248,
                                }}
                            />
                        </Grid>
                        <Grid item md={12}>
                            <Box
                                sx={{
                                    width: "100%",
                                    borderBottomRightRadius: 15,
                                    height: 248,
                                    position: "relative",
                                }}
                            >
                                <Box
                                    component="img"
                                    src={property.thumbnails[2]}
                                    sx={{
                                        width: "100%",
                                        borderBottomRightRadius: 15,
                                        height: "100%",
                                    }}
                                />
                                <Button
                                    onClick={handleOpenPropDetailsModal}
                                    size="small"
                                    startIcon={<CameraAltIcon />}
                                    variant="contained"
                                    color="info"
                                    sx={{
                                        position: "absolute",
                                        bottom: 24,
                                        left: 24,
                                    }}
                                >
                                    {"Afficher " +
                                        property.thumbnails.length +
                                        " photos"}
                                </Button>
                            </Box>
                        </Grid>
                    </Grid>
                </Grid>
            </Container>
        );
    };

    const PropertySpecs = () => {
        return (
            <Container maxWidth="lg" sx={{ px: { xs: 2, md: 2 } }}>
                <Grid container direction="row" columnSpacing={2}>
                    <Grid item xs={12} md={7}>
                        <PropertySummary />
                        <Divider sx={{ my: 5 }} />
                        <Box>
                            <Typography variant="h4" marginBottom={3}>
                                L'essentiel
                            </Typography>
                            <Stack
                                direction={{ xs: "column", md: "row" }}
                                justifyContent={"space-between"}
                            >
                                <Box>
                                    <Stack
                                        direction={"row"}
                                        //justifyContent={"space-between"}
                                        alignItems={"center"}
                                        spacing={2}
                                        marginBottom={1}
                                    >
                                        <EstateIcon width={30} />
                                        <Typography variant="h6">
                                            {property.type == 2
                                                ? "Maison"
                                                : "Appartement"}
                                        </Typography>
                                    </Stack>
                                    <Stack
                                        direction={"row"}
                                        alignItems={"center"}
                                        spacing={2}
                                        marginBottom={1}
                                    >
                                        <ArchiIcon width={30} />
                                        <Typography variant="h6">
                                            {property.properties.room} pièces
                                        </Typography>
                                    </Stack>
                                </Box>
                                <Box>
                                    <Stack
                                        direction={"row"}
                                        alignItems={"center"}
                                        spacing={2}
                                        marginBottom={1}
                                    >
                                        <PlanIcon width={30} />
                                        <Typography variant="h6">
                                            {property.properties.surface}{" "}
                                            habitable
                                        </Typography>
                                    </Stack>

                                    <Stack
                                        direction={"row"}
                                        alignItems={"center"}
                                        spacing={2}
                                        marginBottom={1}
                                    >
                                        <PlanIcon width={30} />
                                        <Typography variant="h6">
                                            {
                                                property.properties[
                                                    "surface-land"
                                                ]
                                            }{" "}
                                            de terrain
                                        </Typography>
                                    </Stack>
                                </Box>
                                <Box>
                                    <Stack
                                        direction={"row"}
                                        alignItems={"center"}
                                        spacing={2}
                                        marginBottom={1}
                                    >
                                        <BedroomIcon width={30} />
                                        <Typography variant="h6">
                                            {property.properties.bedroom}{" "}
                                            chambres
                                        </Typography>
                                    </Stack>
                                    <Stack
                                        direction={"row"}
                                        alignItems={"center"}
                                        spacing={2}
                                    >
                                        <BathIcon width={30} />
                                        <Typography variant="h6">
                                            {property.properties.sDB ||
                                                property.properties.sDE}{" "}
                                            salle de bain/d'eau
                                        </Typography>
                                    </Stack>
                                </Box>
                            </Stack>
                        </Box>

                        <Divider sx={{ my: 5 }} />
                    </Grid>
                    <Grid
                        item
                        md={1}
                        sx={{ display: { xs: "none", sm: "none", md: "flex" } }}
                    ></Grid>
                    <Grid
                        item
                        xs={12}
                        md={4}
                        sx={{ display: { xs: "none", sm: "none", md: "flex" } }}
                    >
                        <Box>
                            <PropertyAgencyCard />
                        </Box>
                    </Grid>
                </Grid>
                <PropertyCharacteristics property={property} />
            </Container>
        );
    };

    return (
        <>
            <PageWrapper title={property.name}>
                <NavLinkWrapper />
                <PropertyImagesWrapper />
                <PropertySpecs />
                <PropertyEnergy
                    dpe={property.properties.consoEner}
                    dpeVal={property.properties.consoEnerg}
                    ges={property.properties.gES}
                    gesVal={property.properties.valeurGES}
                />
            </PageWrapper>
            <PropertyDetailsModal
                openPropDetailsModal={openPropDetailsModal}
                handleClosePropDetailsModal={handleClosePropDetailsModal}
            >
                <RenderCarousel />
            </PropertyDetailsModal>
        </>
    );
}
