import React from "react";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

export default function Heading({ header, subHeader }) {
    return (
        <Box sx={{ px: { xs: 0, sm: 8, md: 0 } }}>
            <Typography
                variant="body1"
                sx={{
                    position: "relative",
                    marginLeft: "40px",
                    textTransform: "uppercase",
                    mb: 2,
                    "&:before": {
                        content: '""',
                        position: "absolute",
                        top: "9px",
                        left: "-40px",
                        width: "25px",
                        height: "2px",
                        bgcolor: "secondary.main",
                    },
                }}
            >
                {subHeader}
            </Typography>
            <Typography variant="h2" sx={{ maxWidth: 500 }}>
                {header}
            </Typography>
        </Box>
    );
}
