import React from "react";
import { Grid, Box, Typography, Stack, Container } from "@mui/material";

export default function PropertyEnergy({ dpe, dpeVal, ges, gesVal }) {
    const energySvgPath = require.context("../../../assets/svg/");
    let gesRt;
    let dpeLt;

    let gesTop = 70;
    let dpeTop = 70;

    switch (ges) {
        case "A":
            gesRt = -15;
            break;
        case "B":
            gesRt = 28;
            break;
        case "C":
            gesRt = 77;
            break;
        case "D":
            gesRt = 122;
            break;
        case "E":
            gesRt = 169;
            break;
        case "F":
            gesRt = 210;
            break;
        case "G":
            gesRt = 256;
            break;

        default:
            break;
    }

    switch (dpe) {
        case "A":
            dpeLt = -8;
            break;
        case "B":
            dpeLt = 39;
            break;
        case "C":
            dpeLt = 86;
            break;
        case "D":
            dpeLt = 134;
            break;
        case "E":
            dpeLt = 180;
            break;
        case "F":
            dpeLt = 224;
            break;
        case "G":
            dpeLt = 269;
            break;

        default:
            break;
    }

    return (
        <Container maxWidth="lg" sx={{ px: { xs: 2, md: 2 }, pb: 10 }}>
            <Typography variant="h4" marginBottom={3}>
                Bilan énergétique
            </Typography>
            <Grid container rowSpacing={5} columnSpacing={2}>
                <Grid item xs={12} sm={12} md={6}>
                    <Typography variant="h5">
                        Consommation énergétique.
                    </Typography>
                    <Box
                        sx={{
                            position: "relative",
                            width: 320,
                            height: "auto",
                            mt: 4,
                        }}
                    >
                        <img
                            src={energySvgPath(`./DPE_${dpe}.svg`)}
                            alt=""
                            width="100%"
                            height="auto"
                        />
                        <Box
                            sx={{
                                position: "absolute",
                                top: dpeTop,
                                left: dpeLt,
                                textAlign: "center",
                            }}
                        >
                            <Stack
                                direction={"column"}
                                spacing={0}
                                alignItems={"center"}
                            >
                                <Typography variant="h4" color="primary">
                                    {dpeVal}
                                </Typography>
                                <Typography variant="caption">
                                    kWh/m².an
                                </Typography>
                            </Stack>
                        </Box>
                    </Box>
                    <Stack direction={"column"} marginTop={8}>
                        <Typography variant="caption" color="success.dark">
                            Logement très performant.
                        </Typography>
                        <Typography variant="caption" color="warning.dark">
                            Logement extrement peu performant.
                        </Typography>
                    </Stack>
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                    <Typography variant="h5">Emission de gaz.</Typography>
                    <Box
                        sx={{
                            position: "relative",
                            width: 320,
                            height: "auto",
                            mt: 4,
                        }}
                    >
                        <img
                            src={energySvgPath(`./GES_${ges}.svg`)}
                            alt=""
                            width="100%"
                            height="auto"
                            style={{ objectFit: "cover" }}
                        />
                        <Box
                            sx={{
                                position: "absolute",
                                top: gesTop,
                                left: gesRt,
                                textAlign: "center",
                            }}
                        >
                            <Stack
                                direction={"column"}
                                spacing={0}
                                alignItems={"center"}
                            >
                                <Typography variant="h4" color="primary">
                                    {gesVal}
                                </Typography>
                                <Typography variant="caption">
                                    kg.CO2/m².an
                                </Typography>
                            </Stack>
                        </Box>
                    </Box>
                    <Stack direction={"column"} marginTop={8}>
                        <Typography variant="caption" color="info.darker">
                            Emissions de CO2 moindre.
                        </Typography>
                        <Typography variant="caption" color="primary.main">
                            Emissions de CO2 importantes.
                        </Typography>
                    </Stack>
                </Grid>
            </Grid>
        </Container>
    );
}
