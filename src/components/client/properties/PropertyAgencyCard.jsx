import React from "react";
import {
    TextField,
    Card,
    CardContent,
    CardHeader,
    CardActions,
    Box,
    Button,
    Typography,
} from "@mui/material";
import { ReactComponent as Logo } from "../../../assets/images/logo.svg";
import styled from "@emotion/styled";

export default function PropertyAgencyCard() {
    const CardInput = styled(TextField)(() => ({
        marginBottom: "1.5rem",
        "& fieldset": {
            borderRadius: "20px",
        },
    }));

    return (
        <Card sx={{ mt: 5, pb: 1, bgcolor: 'common.white', boxShadow: 3 }}>
            <Box sx={{ textAlign: "center", pt: 4 }}>
                <Logo />
            </Box>
            <CardContent>
                <Box marginBottom={5}>
                    <Typography variant="h5" fontWeight={700}>
                        Ce bien vous interesse ?
                    </Typography>
                    <Typography>
                        Contacter vite l'agence pour une visite.
                    </Typography>
                </Box>
                <CardInput fullWidth type="text" label="Nom" required />
                <CardInput
                    fullWidth
                    type="email"
                    label="Email"
                    variant="outlined"
                    required
                />
                <CardInput
                    fullWidth
                    multiline
                    label="Message"
                    variant="outlined"
                    rows={4}
                    required
                />

                <Button
                    variant="contained"
                    fullWidth
                    color="secondary"
                    sx={{ py: 2 }}
                >
                    Contacter l'agence
                </Button>
            </CardContent>
        </Card>
    );
}
