import React from "react";
import { Skeleton, Grid } from "@mui/material";

export default function CardItemSkeletons({ cardNum }) {
    const CardSkeleton = () => {
        return (
            <Grid item xs={12} sm={6} md={6}>
                <Skeleton
                    variant="rectangle"
                    height={200}
                    animation="wave"
                    sx={{ mb: 2 }}
                />
                <Skeleton variant="rectangle" animation="wave" />
            </Grid>
        );
    };

    return Array.from(Array(cardNum)).map((card, index) => {
        return <CardSkeleton key={index} />;
    });
}
