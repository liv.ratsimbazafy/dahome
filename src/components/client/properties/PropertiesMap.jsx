import Map, { Marker, Popup, NavigationControl } from "react-map-gl";
import mapboxgl from "mapbox-gl";
import React, { useEffect, useState } from "react";
import { Link as RouterLink } from "react-router-dom";
import {
    IconButton,
    Card,
    CardMedia,
    CardContent,
    Typography,
    Stack,
    Box,
    Link,
} from "@mui/material";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import { useSelector } from "react-redux";
import PropertyCard from "./PropertyCard";
// eslint-disable-next-line import/no-webpack-loader-syntax
mapboxgl.workerClass = require("worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker").default;

const MarkerStyle = ({ properties, getSelectedItem, markerColor }) => {
    return properties.map((property) => (
        <Marker
            key={property.id}
            latitude={property.location.lat}
            longitude={property.location.lon}
            //anchor="top"
        >
            <IconButton
                aria-label="location"
                onClick={(e) => getSelectedItem(property, e)}
            >
                <LocationOnIcon
                    sx={{
                        fontSize: 50,
                        color:
                            markerColor !== null && markerColor === property.id
                                ? "#F0B212"
                                : "#0A172A",
                    }}
                />
            </IconButton>
        </Marker>
    ));
};

const PopupCard = ({ property }) => (
    <Link component={RouterLink} to={`/buy/${property.id}`} underline="none">
        <Card sx={{ width: 350 }}>
            <CardMedia
                component="img"
                height="180"
                image={property.thumbnails[0]}
                alt={property.title}
            />
            <CardContent sx={{ minHeight: 100 }}>
                <Stack
                    direction="row"
                    justifyContent="space-between"
                    alignItems="flex-start"
                    spacing={2}
                    marginBottom={1}
                >
                    <Box>
                        <Typography variant="body1" fontWeight={500}>
                            {property.properties.typeBien}
                        </Typography>
                        <Typography variant="body1" color="grey.700">
                            {property.properties.room} pièces -{" "}
                            {property.properties.surface}
                        </Typography>
                    </Box>
                    <Typography
                        variant="h6"
                        sx={{ lineHeight: 1, fontWeight: 700 }}
                    >
                        € {property.price}
                    </Typography>
                </Stack>
                <Typography
                    variant="body1"
                    color="grey.700"
                    sx={{ textTransform: "capitalize" }}
                >
                    {property.properties.city.toLowerCase()}
                </Typography>
            </CardContent>
        </Card>
    </Link>
);

const PopupStyle = ({ property, setSelectedCity, setMarkerColor }) => {
    if (!property) return null;

    return (
        <Popup
            longitude={property.location.lon}
            latitude={property.location.lat}
            anchor="none"
            onClose={() => {
                setSelectedCity(null);
                setMarkerColor(null);
            }}
            offset={30}
            maxWidth={350}            
            style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}
        >
            <PopupCard property={property} />
        </Popup>
    );
};

export default function PropertiesMap({
    properties,
    getSelectedItem,
    setSelectedCity,
    setMarkerColor,
    selectedCity,
    markerColor,
}) {
    const { geometry } = useSelector((state) => state.dahome);
    const [view, setView] = useState({
        longitude: geometry.lng,
        latitude: geometry.lat,
        zoom: 12,
        pitch: 20,
    });

    if (properties.length === 0) return false;
    return (
        <Map
            style={{ borderRadius: "20px", position: "relative" }}
            initialViewState={{
                ...view,
            }}
            onViewportChange={(evt) => setView(evt.view)}
            mapboxAccessToken="pk.eyJ1IjoibGltamFoIiwiYSI6ImNsOGJwNnZlNjBlMDUzb3BjcXQ1cG9pdHAifQ.4VvgJjg-bXCxEqR_7aFrjw"
            mapStyle="mapbox://styles/limjah/cl8n8fu57004015mtyfpmbk4r"
        >
            <MarkerStyle
                properties={properties}
                getSelectedItem={getSelectedItem}
                markerColor={markerColor}
            />

            <PopupStyle
                property={selectedCity}
                setSelectedCity={setSelectedCity}
                setMarkerColor={setMarkerColor}
            />

            <NavigationControl />
        </Map>
    );
}
