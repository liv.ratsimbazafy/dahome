import React, { useState } from "react";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import { useDispatch, useSelector } from "react-redux";
import {
    updateFilter,
    callFilters,
    removeFilter,
} from "../../../../store/actions/propertiesActions";

const roomList = ["studio", "2", "3", "4", "5"];

export default function RoomFilter() {
    const { customFilters } = useSelector((state) => state.dahome);
    const dispatch = useDispatch();
    const [room, setRoom] = useState([]);

    const handleRoomChange = (e, selectedItem) => {
        e.preventDefault();
        if (!room.includes(selectedItem)) {
            setRoom(selectedItem);
            dispatch(
                updateFilter({
                    type: "nested",
                    value: { room: parseInt(selectedItem) },
                })
            );
            dispatch(callFilters());
        } else {
            setRoom([]);
            dispatch(removeFilter(["properties", "room"]));
            dispatch(callFilters());
        }
    };

    return (
        <Box>
            <Typography variant="body1" fontWeight={700}>
                Combien de pièces ?
            </Typography>
            <Stack
                direction="row"
                spacing={2}
                mt={2}
                sx={{
                    overflow: "auto",
                    "&::-webkit-scrollbar": { display: "none" },
                }}
            >
                {roomList.map((item, i) => {
                    return (
                        <Button
                            key={i}
                            onClick={(e) => handleRoomChange(e, item)}
                            variant={
                                room.includes(item) ||
                                customFilters.properties.room == item
                                    ? "contained"
                                    : "outlined"
                            }
                            size="small"
                        >
                            {item}
                        </Button>
                    );
                })}
            </Stack>
        </Box>
    );
}
