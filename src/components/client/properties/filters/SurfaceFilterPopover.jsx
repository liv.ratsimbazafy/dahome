import * as Yup from "yup";
import React, { Fragment, useState, useRef, useEffect } from "react";
import { useFormik, Form, FormikProvider } from "formik";
import { useSelector } from "react-redux";
import {
    Typography,
    Box,
    Button,
    InputAdornment,
    Divider,
    TextField,
} from "@mui/material";
import MenuPopover from "../../MenuPopover";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";
import {
    updateFilter,
    callFilters,
    removeFilter,
    setSelectedFilters,
    removeSelectedFilter,
} from "../../../../store/actions/propertiesActions";
import { useDispatch } from "react-redux";

export default function SurfaceFilterPopover() {
    const { customFilters, selectedFilters } = useSelector(
        (state) => state.dahome
    );
    const dispatch = useDispatch();
    const anchorRef = useRef(null);
    const [open, setOpen] = useState(false);

    useEffect(() => {
        if (!selectedFilters.includes("surface"))
            formik.resetForm({
                surfaceMin: "",
            });
    }, [selectedFilters]);

    const surfaceErrorMsg = {
        integer: "Nombres uniquement!",
        required: "Champ obligatoire!",
        positive: "Positive uniquement!",
    };

    const SurfaceSchema = Yup.object().shape({
        surfaceMin: Yup.number()
            .typeError(surfaceErrorMsg.integer)
            .required(surfaceErrorMsg.required)
            .positive(surfaceErrorMsg.positive),
        // surfaceMax: Yup.number()
        //     .typeError(surfaceErrorMsg.integer)
        //     .required(surfaceErrorMsg.required)
        //     .positive(surfaceErrorMsg.positive),
    });

    const formik = useFormik({
        initialValues: {
            surfaceMin: "",
            //surfaceMax: "",
        },
        validationSchema: SurfaceSchema,
        onSubmit: (values, { setSubmitting }) => {
            handleSurfaceSubmit(values);
        },
    });

    const { errors, values, isSubmitting, handleSubmit, getFieldProps } =
        formik;

    const openPopover = () => {
        setOpen(true);
    };

    const closePopover = () => {
        setOpen(false);
    };

    const surfaceReset = () => {
        dispatch(removeFilter(["properties", "surface"]));
        dispatch(removeSelectedFilter("surface"));
        dispatch(callFilters({}));
        return formik.resetForm({
            surfaceMin: "",
            //surfaceMax: "",
        });
    };

    const handleSurfaceSubmit = (formValues) => {
        const payload = {
            type: "nested",
            value: { surface: formValues.surfaceMin },
        };
        dispatch(updateFilter(payload));
        dispatch(setSelectedFilters("surface"));
        dispatch(callFilters());
        setOpen(false);
    };

    return (
        <Fragment>
            <Button
                variant="outlined"
                endIcon={open ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />}
                ref={anchorRef}
                onClick={openPopover}
                sx={{ minWidth: 150, textTransform: "lowercase" }}
            >
                {formik.values.surfaceMin
                    ? "Min: " + formik.values.surfaceMin + " m²"
                    : "Surface"}
            </Button>

            <MenuPopover
                open={open}
                onClose={closePopover}
                anchorEl={anchorRef.current}
                sx={{ width: 250, my: 1.5, px: 2.5, pt: 3, pb: 1 }}
            >
                <Box>
                    <Typography variant="subtitle1" noWrap fontWeight={"bold"}>
                        Quelle surface au minimum ?
                    </Typography>
                </Box>
                <FormikProvider value={formik}>
                    <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <Box sx={{ py: 4, display: "flex", gap: 2 }}>
                            <Box>
                                <TextField
                                    variant="outlined"
                                    size="small"
                                    placeholder="Min"
                                    {...getFieldProps("surfaceMin")}
                                    error={Boolean(errors.surfaceMin)}
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                m²
                                            </InputAdornment>
                                        ),
                                        inputProps: { maxLength: 4 },
                                    }}
                                />
                                {errors && (
                                    <Typography
                                        variant="caption"
                                        color="primary"
                                    >
                                        {errors.surfaceMin}
                                    </Typography>
                                )}
                            </Box>
                            {/* <Box>
                                <TextField
                                    variant="outlined"
                                    size="small"
                                    placeholder="Max"
                                    {...getFieldProps("surfaceMax")}
                                    error={Boolean(errors.surfaceMax)}
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                m²
                                            </InputAdornment>
                                        ),
                                        inputProps: { maxLength: 4, min: 1 },
                                    }}
                                />
                                {errors && (
                                    <Typography
                                        variant="caption"
                                        color="primary"
                                        sx={{ lineHeight: 1 }}
                                    >
                                        {errors.surfaceMax}
                                    </Typography>
                                )}
                            </Box> */}
                        </Box>

                        <Divider />

                        <Box
                            display={"flex"}
                            justifyContent={"space-between"}
                            sx={{ mt: 1 }}
                        >
                            <Button
                                size="small"
                                variant="text"
                                onClick={surfaceReset}
                                sx={{ color: "text.secondary" }}
                            >
                                Réinitialiser
                            </Button>
                            <Button
                                size="small"
                                variant="text"
                                type="submit"
                                disabled={
                                    !values.surfaceMin ||
                                    Boolean(errors.surfaceMin)
                                }
                            >
                                Valider
                            </Button>
                        </Box>
                    </Form>
                </FormikProvider>
            </MenuPopover>
        </Fragment>
    );
}
