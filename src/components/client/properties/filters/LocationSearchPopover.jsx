import React, { Fragment, useState, useRef } from "react";
import { Box, Button } from "@mui/material";
import MenuPopover from "../../MenuPopover";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";
import { useSelector } from "react-redux";

export default function LocationSearchPopover({
    children,
    open,
    handleLocPopover,
}) {
    const anchorRef = useRef(null);
    const { customFilters } = useSelector((state) => state.dahome);

    return (
        <Fragment>
            <Button
                variant={"contained"}
                endIcon={open ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />}
                ref={anchorRef}
                onClick={handleLocPopover}
            >
                {customFilters.properties.city
                    ? customFilters.properties.city.toLowerCase()
                    : "Localisation"}
            </Button>

            <MenuPopover
                open={open}
                onClose={handleLocPopover}
                anchorEl={anchorRef.current}
                anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
                transformOrigin={{ vertical: "top", horizontal: "left" }}
                sx={{ width: 400, my: 1.5, px: 2.5, pt: 3, pb: 1 }}
            >
                <Box sx={{ pb: 3 }}>{children}</Box>
            </MenuPopover>
        </Fragment>
    );
}
