import React from "react";
import numeral from "numeral";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import Slider from "@mui/material/Slider";
import {
    updateFilter,
    setSelectedFilters,
    callFilters,
} from "../../../../store/actions/propertiesActions";
import { useDispatch, useSelector } from "react-redux";

function calculateValue(value) {
    return 1000 * value;
}

export function PriceSliderFilter() {
    const { customFilters } = useSelector((state) => state.dahome);
    const dispatch = useDispatch();

    let statePriceMin = 50;
    const [sliderValue, setSliderValue] = React.useState(() => {
        if (customFilters.properties.hasOwnProperty("price")) {
            statePriceMin = parseInt(
                customFilters.properties.price.toString().slice(0, -3)
            );
            return [5, statePriceMin];
        }
        return [5, statePriceMin];
    });

    const handleSliderChange = (event, newValue) => {
        setSliderValue(newValue);
        const payload = {
            type: "nested",
            value: { price: calculateValue(newValue[1]) },
        };
        dispatch(updateFilter(payload));
        dispatch(setSelectedFilters("prix"));
        dispatch(callFilters());
    };
    return (
        <Box>
            <Typography variant="body1" fontWeight={700}>
                Quel est votre budget ?
            </Typography>
            <Typography>
                Entre {numeral(sliderValue[0] * 1000).format(0.0)}€ et{" "}
                {numeral(sliderValue[1] * 1000).format(0.0)}€
            </Typography>
            <Stack direction="row" spacing={2} mt={2} alignItems={"center"}>
                <Slider
                    getAriaLabel={() => "Temperature range"}
                    value={sliderValue}
                    onChange={handleSliderChange}
                    valueLabelDisplay="auto"
                    disableSwap
                    min={10}
                    max={1000}
                    step={5}
                    scale={calculateValue}
                />
            </Stack>
        </Box>
    );
}
