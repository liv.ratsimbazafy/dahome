import React, { useState, useEffect } from "react";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Divider from "@mui/material/Divider";
import Autocomplete from "@mui/material/Autocomplete";
import RoomFilter from "./RoomFilter";
import { PriceSliderFilter } from "./PriceSliderFilter";
import { useDispatch, useSelector } from "react-redux";
import SurfaceDialogFilter from "./SurfaceDialogFilter";
import { getSelectedPropertyType } from "../../../../helpers/filters";
import { formatCity } from "../../../../helpers/formatCity";
import { deptsAutocomplete } from "../../../../api/search";
import {
    updateFilter,
    updateGeometry,
    cityFilter,
    callFilters,
} from "../../../../store/actions/propertiesActions";
import { useTheme } from "@mui/material/styles";
import useMediaQuery from "@mui/material/useMediaQuery";

export default function FilterModal({
    openFilterModal,
    handleCloseFilterModal,
    checkboxData,
    handlePropertyTypeChange,
    typeboxItems,
    setLoading,
}) {
    const { customFilters, filteredProperties } = useSelector(
        (state) => state.dahome
    );
    const theme = useTheme();
    const fullScreenDialog = useMediaQuery(theme.breakpoints.down("sm"));

    const dispatch = useDispatch();
    const [scroll, setScroll] = React.useState("paper");
    const [selectedGeometry, setSelectedGeometry] = useState({});
    const [departments, setDepartments] = useState([]);

    useEffect(() => {
        dispatch(updateFilter({ type: getSelectedPropertyType(checkboxData) }));
        dispatch(callFilters({}));
    }, [checkboxData]);

    const extractCityGeolocation = async (location) => {
        const res = await departments.find((city) => {
            return city.properties.label === location.substring(8);
        });

        res !== undefined &&
            setSelectedGeometry({
                lat: res.geometry.coordinates[1],
                lng: res.geometry.coordinates[0],
            });
        return dispatch(
            updateGeometry({
                lat: res.geometry.coordinates[1],
                lng: res.geometry.coordinates[0],
            })
        );
    };

    const handleCityChange = (e, newCity) => {
        if (newCity === null) return;
        setLoading(true);
        extractCityGeolocation(newCity);
        dispatch(cityFilter(formatCity(newCity)));
        dispatch(callFilters({}));
    };

    const handleAutoCompletion = async (e) => {
        if (e.target.value.length < 4) return;
        const { data } = await deptsAutocomplete(e.target.value);
        setDepartments(data.features);
    };

    return (
        <div>
            <Dialog
                fullScreen={fullScreenDialog}
                fullWidth
                open={openFilterModal}
                onClose={handleCloseFilterModal}
                scroll={scroll}
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
            >
                <DialogTitle
                    sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                    }}
                >
                    <Typography variant="body1" fontWeight={700}>
                        Filtres
                    </Typography>
                    <IconButton
                        aria-label="close"
                        size="small"
                        onClick={handleCloseFilterModal}
                    >
                        <CloseIcon />
                    </IconButton>
                </DialogTitle>
                <DialogContent dividers={scroll === "paper"}>
                    {/* Transaction filter */}
                    <Box>
                        <Typography variant="body1" fontWeight={700}>
                            Vous souhaitez ?
                        </Typography>
                        <Stack direction="row" spacing={2} mt={2}>
                            <Button
                                variant={
                                    customFilters.target === "buy"
                                        ? "contained"
                                        : "outlined"
                                }
                                size="small"
                            >
                                Acheter
                            </Button>
                            <Button variant="outlined" size="small">
                                Louer
                            </Button>
                        </Stack>
                    </Box>
                    <Divider sx={{ my: 3 }} />
                    {/* property type filter */}
                    <Box>
                        <Typography variant="body1" fontWeight={700}>
                            Quel type de bien ?
                        </Typography>
                        <Stack
                            direction="row"
                            spacing={0}
                            mt={2}
                            sx={{
                                overflow: "auto",
                                "&::-webkit-scrollbar": { display: "none" },
                            }}
                        >
                            {typeboxItems.map((item, index) => (
                                <FormControlLabel
                                    key={index}
                                    control={
                                        <Checkbox
                                            size="small"
                                            checked={checkboxData[index]}
                                            onClick={() =>
                                                handlePropertyTypeChange(index)
                                            }
                                        />
                                    }
                                    label={item.label}
                                />
                            ))}
                        </Stack>
                    </Box>
                    <Divider sx={{ my: 3 }} />
                    {/* city filter */}
                    <Box>
                        <Typography variant="body1" fontWeight={700}>
                            A quel endroit ?
                        </Typography>
                        <Stack direction="row" spacing={2} mt={2}>
                            <Autocomplete
                                size="small"
                                sx={{ minWidth: 280 }}
                                filterOptions={(x) => x}
                                onClose={() => setDepartments([])}
                                noOptionsText={
                                    "Veuillez entrer une ville ou un département"
                                }
                                onChange={(e, val) => handleCityChange(e, val)}
                                options={
                                    departments
                                        ? departments.map(
                                              (option) =>
                                                  option.properties.postcode +
                                                  " - " +
                                                  option.properties.city
                                          )
                                        : []
                                }
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        label="Où souhaitez-vous louer?"
                                        onChange={(e) =>
                                            handleAutoCompletion(e)
                                        }
                                    />
                                )}
                                isOptionEqualToValue={(option, value) =>
                                    option.id === value.id
                                } // remove  warning value provided to Autocomplete is invalid
                            />
                        </Stack>
                    </Box>
                    <Divider sx={{ my: 3 }} />
                    {/* price filter */}
                    <PriceSliderFilter />

                    <Divider sx={{ my: 3 }} />
                    {/* surface filter */}
                    <SurfaceDialogFilter />
                    <Divider sx={{ my: 3 }} />
                    {/* room filter */}
                    <RoomFilter />
                </DialogContent>
                <DialogActions
                    sx={{ display: "flex", justifyContent: "space-between" }}
                >
                    <Button onClick={handleCloseFilterModal} variant="text">
                        Tout effacer
                    </Button>
                    <Button
                        variant="contained"
                        onClick={handleCloseFilterModal}
                    >
                        {filteredProperties.length != 0
                            ? "afficher " + filteredProperties.length + " biens"
                            : "Aucun résultat"}
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
