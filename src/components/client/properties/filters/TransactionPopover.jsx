import React, { Fragment, useState, useRef } from "react";

import { Typography, Box, Button, Divider } from "@mui/material";
import MenuPopover from "../../MenuPopover";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";
import buy from "../../../../assets/images/buy.svg";
import rent from "../../../../assets/images/rent.svg";

export default function TransactionPopover() {
    const anchorRef = useRef(null);
    const [open, setOpen] = useState(false);
    const [transaction, setTransaction] = useState("Acheter");
    const buyRef = useRef(null);
    const rentRef = useRef(null);

    const openPopover = () => {
        setOpen(true);
    };

    const closePopover = () => {
        setOpen(false);
    };

    const handleTransaction = (e) => {
        setTransaction(e.target.dataset.transaction);
    };

    return (
        <Fragment>
            <Button
                //size="large"
                variant="outlined"
                endIcon={open ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />}
                ref={anchorRef}
                onClick={openPopover}
                //sx={{ minWidth: 120, py: 3.4 }}
            >
                {transaction}
            </Button>

            <MenuPopover
                open={open}
                onClose={closePopover}
                anchorEl={anchorRef.current}
                anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
                transformOrigin={{ vertical: "top", horizontal: "left" }}
                sx={{ width: 300, my: 1.5, px: 2.5, pt: 3, pb: 1 }}
            >
                <Box>
                    <Typography variant="subtitle1" noWrap>
                        Vous souhaiter ?
                    </Typography>
                </Box>

                <Box sx={{ py: 4, display: "flex", gap: 2 }}>
                    <Box
                        ref={buyRef}
                        data-transaction="Acheter"
                        onClick={handleTransaction}
                        sx={{
                            p: 2,
                            ...(transaction === "Acheter"
                                ? {
                                      border: "1px solid red",
                                  }
                                : { border: "1px solid #000" }),
                            cursor: "pointer",
                            borderRadius: 2,
                            zIndex: 100,
                        }}
                    >
                        <img
                            src={buy}
                            width="87"
                            height="65"
                            data-transaction="Acheter"
                        />
                        <Typography
                            variant="body2"
                            textAlign={"center"}
                            data-transaction="Acheter"
                        >
                            Acheter
                        </Typography>
                    </Box>
                    <Box
                        data-transaction="Louer"
                        onClick={handleTransaction}
                        ref={rentRef}
                        sx={{
                            p: 2,
                            cursor: "pointer",
                            borderRadius: 2,
                            ...(transaction === "Louer"
                                ? {
                                      border: "1px solid red",
                                  }
                                : { border: "1px solid #000" }),
                        }}
                    >
                        <img
                            src={rent}
                            width="87"
                            height="65"
                            data-transaction="Louer"
                        />
                        <Typography
                            variant="body2"
                            textAlign={"center"}
                            data-transaction="Louer"
                        >
                            Louer
                        </Typography>
                    </Box>
                </Box>

                <Divider />

                <Box
                    display={"flex"}
                    justifyContent={"space-between"}
                    sx={{ mt: 1 }}
                >
                    <Button variant="text">Réinitialiser</Button>
                    <Button variant="text" onClick={() => setOpen(false)}>
                        Valider
                    </Button>
                </Box>
            </MenuPopover>
        </Fragment>
    );
}
