import React, { useState } from "react";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import InputAdornment from "@mui/material/InputAdornment";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import {
    updateFilter,
    setSelectedFilters,
    callFilters,
    removeFilter,
} from "../../../../store/actions/propertiesActions";
import { useDispatch } from "react-redux";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";

export default function SurfaceDialogFilter() {
    const dispatch = useDispatch();
    const [sMin, setSMin] = useState("");
    const [sMax, setSMax] = useState("");
    const [displayVal, setDisplayVal] = useState(false);

    const handleSMin = (e) => {
        setDisplayVal(false);
        setSMin(e.target.value);
    };
    const handleSMax = (e) => {
        setDisplayVal(false);
        setSMax(e.target.value);
    };

    const handleSurfaceSubmit = () => {
        const payload = {
            type: "nested",
            value: { surface: sMax },
        };
        dispatch(updateFilter(payload));
        dispatch(setSelectedFilters("surface"));
        dispatch(callFilters());
        setDisplayVal(true);
    };

    const removeSurfaceFilter = () => {
        dispatch(removeFilter(["properties", "surface"]));
        dispatch(callFilters());
        setDisplayVal(false);
        setSMin("");
        setSMax("");
    };

    return (
        <Box>
            <Typography variant="body1" fontWeight={700}>
                Quel surface ?
            </Typography>
            <Typography>
                {displayVal && `Entre ${sMin}m² à  ${sMax}m²`}
            </Typography>
            <Stack direction="row" spacing={2} mt={2} alignItems={"center"}>
                <TextField
                    type="number"
                    size="small"
                    label="Min"
                    sx={{ width: 150 }}
                    value={sMin}
                    onChange={(e) => handleSMin(e)}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">m²</InputAdornment>
                        ),
                        inputProps: { min: 5, max: 1000 },
                    }}
                />
                <TextField
                    type="number"
                    min="0"
                    max="100"
                    size="small"
                    label="Max"
                    sx={{ width: 150 }}
                    value={sMax}
                    onChange={(e) => handleSMax(e)}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">m²</InputAdornment>
                        ),
                        inputProps: { min: 5, max: 1000 },
                    }}
                />
                {displayVal ? (
                    <IconButton
                        aria-label="check"
                        color={"primary"}
                        onClick={removeSurfaceFilter}
                    >
                        <RemoveCircleOutlineIcon />
                    </IconButton>
                ) : (
                    <IconButton
                        aria-label="check"
                        color={"primary"}
                        onClick={handleSurfaceSubmit}
                        disabled={!sMax}
                    >
                        <CheckCircleOutlineIcon />
                    </IconButton>
                )}
            </Stack>
        </Box>
    );
}
