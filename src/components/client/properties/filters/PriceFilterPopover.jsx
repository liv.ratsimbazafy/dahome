import * as Yup from "yup";
import React, { Fragment, useState, useRef, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useFormik, Form, FormikProvider } from "formik";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import InputAdornment from "@mui/material/InputAdornment";
import Divider from "@mui/material/Divider";
import TextField from "@mui/material/TextField";
import MenuPopover from "../../MenuPopover";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";
import {
    callFilters,
    removeFilter,
    updateFilter,
    setSelectedFilters,
    removeSelectedFilter,
} from "../../../../store/actions/propertiesActions";

export default function PriceFilterPopover() {
    const dispatch = useDispatch();
    const { customFilters, selectedFilters } = useSelector(
        (state) => state.dahome
    );
    const anchorRef = useRef(null);
    const [open, setOpen] = useState(false);
    const [priceState, setPriceState] = React.useState(() => {
        if (
            customFilters.properties.hasOwnProperty("price") ||
            customFilters.properties.price != undefined
        ) {
            return customFilters.properties.price;
        }
    });

    useEffect(() => {
        if (!selectedFilters.includes("prix"))
            formik.resetForm({
                priceMax: "",
            });
        if (priceState != customFilters.properties.price)
            setPriceState(customFilters.properties.price);
    }, [selectedFilters]);

    const priceErrorMsg = {
        integer: "Nombres uniquement!",
        required: "Champ obligatoire!",
        positive: "Positive uniquement!",
    };

    const PriceSchema = Yup.object().shape({
        priceMax: Yup.number()
            .typeError(priceErrorMsg.integer)
            .required(priceErrorMsg.required)
            .positive(priceErrorMsg.positive),
    });

    const formik = useFormik({
        enableReinitialize: true,
        initialValues: {
            priceMax: priceState ? priceState : "",
        },
        validationSchema: PriceSchema,
        onSubmit: (values, { setSubmitting }) => {
            handlePriceSubmit(values);
        },
    });

    const { errors, values, isSubmitting, handleSubmit, getFieldProps } =
        formik;

    const openPopover = () => {
        setOpen(true);
    };

    const closePopover = () => {
        setOpen(false);
    };

    const formReset = () => {
        dispatch(removeFilter(["properties", "price"]));
        dispatch(removeSelectedFilter("prix"));
        dispatch(callFilters({}));
        return formik.resetForm({
            priceMax: "",
        });
    };

    const handlePriceSubmit = (formValues) => {
        const payload = {
            type: "nested",
            value: { price: formValues.priceMax },
        };
        dispatch(updateFilter(payload));
        dispatch(setSelectedFilters("prix"));
        dispatch(callFilters());
        setOpen(false);
    };

    const FormatPrice = () => {
        let price = formik.values.priceMax;

        if (priceState !== undefined && String(priceState).length > 4)
            price = String(priceState);

        return price.length > 4
            ? "Max - " + price.slice(0, -3) + "K"
            : price
            ? price
            : "Max";
    };

    return (
        <Fragment>
            <Button
                color={"primary"}
                variant={formik.values.priceMax ? "contained" : "outlined"}
                endIcon={open ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />}
                ref={anchorRef}
                onClick={openPopover}
                sx={{ minWidth: 110 }}
            >
                {formik.values.priceMax || priceState ? (
                    <FormatPrice />
                ) : (
                    "Budget"
                )}
            </Button>

            <MenuPopover
                open={open}
                onClose={closePopover}
                anchorEl={anchorRef.current}
                sx={{ width: 250, my: 1.5, px: 2.5, pt: 3, pb: 1 }}
            >
                <Box>
                    <Typography variant="subtitle1" noWrap fontWeight={"bold"}>
                        Quel est votre budget ?
                    </Typography>
                </Box>

                <FormikProvider value={formik}>
                    <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                        <Box sx={{ py: 4, display: "flex", gap: 2 }}>
                            <Box>
                                <TextField
                                    variant="outlined"
                                    size="small"
                                    {...getFieldProps("priceMax")}
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                €
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                                {errors && (
                                    <Typography
                                        variant="caption"
                                        color="primary"
                                        sx={{ lineHeight: 1 }}
                                    >
                                        {errors.priceMax}
                                    </Typography>
                                )}
                            </Box>
                        </Box>

                        <Divider />

                        <Box
                            display={"flex"}
                            justifyContent={"space-between"}
                            sx={{ mt: 1 }}
                        >
                            <Button
                                size="small"
                                variant="text"
                                onClick={formReset}
                                sx={{ color: "text.secondary" }}
                            >
                                Réinitialiser
                            </Button>
                            <Button
                                size="small"
                                variant="text"
                                type="submit"
                                disabled={
                                    !values.priceMax || Boolean(errors.priceMax)
                                }
                            >
                                Valider
                            </Button>
                        </Box>
                    </Form>
                </FormikProvider>
            </MenuPopover>
        </Fragment>
    );
}
