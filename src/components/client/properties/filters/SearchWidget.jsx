import React, { useState, Fragment, useRef } from "react";
import { useNavigate, createSearchParams } from "react-router-dom";
import axios from "axios";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Tab from "@mui/material/Tab";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import SearchIcon from "@mui/icons-material/Search";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";
import MenuPopover from "../../MenuPopover";

import { useDispatch, useSelector } from "react-redux";
import {
    setFilteredProperties,
    setMainFilters,
} from "../../../../store/actions/propertiesActions";
import { formatCity } from "../../../../helpers/formatCity";
import {
    filterData,
    getSelectedPropertyType,
} from "../../../../helpers/filters";

const widgetWrapperStyle = {
    position: "absolute",
    top: { xs: "60%", md: "65%" },
    left: "50%",
    transform: "translate(-50%, -50%)",
    minWidth: { xs: 320, sm: 420, md: 700 },
    maxWidth: { xs: 320, sm: 420, md: 850 },
    minHeight: 300,
};

const checkBoxList = [
    {
        order: 0,
        name: "Maison",
    },
    {
        order: 1,
        name: "Appartement",
    },
    {
        order: 2,
        name: "Terrain",
    },
];

const tabItems = [
    {
        label: "Acheter",
        value: "1",
        dataType: "buy",
    },
    {
        label: "Louer",
        value: "2",
        dataType: "rent",
    },
    {
        label: "Estimer",
        value: "3",
        dataType: "estimer",
    },
];

export default function SearchWidget({ allData }) {
    const { filteredProperties } = useSelector((state) => state.dahome);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const anchorRef = useRef(null);
    const [tabValue, setTabValue] = useState("1");
    const [departements, setDepartements] = useState([]);
    const [searchQuery, setSearchQuery] = useState(null);
    const [transaction, setTransaction] = useState("buy");
    const [disableBtn, setDisableBtn] = useState(true);
    const [selectedGeometry, setSelectedGeometry] = useState({});
    const [autocompleteVal, setAutocompleteVal] = useState("");
    const [checkboxData, setCheckboxData] = useState([true, false, false]);
    const [filterState, setFilterState] = useState("");
    const [open, setOpen] = useState(false);
    const [noDataMsg, setNoDataMsg] = useState(false);

    const openPopover = () => {
        setOpen(true);
    };

    const closePopover = () => {
        setOpen(false);
    };

    const handleTabsChange = (event, newValue) => {
        setTabValue(newValue);
        setTransaction(event.target.dataset.type);
        resetSearchProcess();
    };

    const fetchDepartement = async (query) => {
        //if (query.length <= 2) return;

        const cooUrl = `https://api-adresse.data.gouv.fr/search/?q=${query}&type=municipality&autocomplete=1`;
        let { data } = await axios.get(cooUrl);
        //console.log(data);

        return data;
    };

    const handleAutoCompletion = async (e) => {
        if (e.target.value.length < 4) return;
        const data = await fetchDepartement(e.target.value);
        setDepartements(data.features);
    };

    const extractCityGeolocation = (location) => {
        const res = departements.find((city) => {
            return city.properties.label === location.substring(8);
        });
        res !== undefined &&
            setSelectedGeometry({
                lat: res.geometry.coordinates[1],
                lng: res.geometry.coordinates[0],
            });
    };

    const onSearchTermChange = (e, cityTerm) => {
        let propertyType = getSelectedPropertyType(checkboxData);
        if (propertyType.length < 1) propertyType = [true, true, false];

        if (cityTerm !== null) {
            setDisableBtn(false);
            setAutocompleteVal(cityTerm);
            setSearchQuery(formatCity(cityTerm));
            extractCityGeolocation(cityTerm);
            checkAvailableProperties(cityTerm, propertyType, allData);
        } else {
            resetSearchProcess();
            return;
        }
    };

    const checkAvailableProperties = (cityVal, propertyType, data) => {
        const filterState = {
            type: propertyType,
            target: transaction,
            properties: {
                city: formatCity(cityVal),
            },
        };

        const res = filterData(data, filterState);
        if (res.length === 0) {
            setDisableBtn(true);
            setNoDataMsg(true);
        }
        setFilterState(filterState);
        dispatch(setFilteredProperties(res));
    };

    const onFormSubmit = () => {
        const testFilters = {
            target: transaction,
            type: "2",
            properties: {
                city: searchQuery,
            },
        };
        let payload = { filters: filterState, geometry: selectedGeometry };
        dispatch(setMainFilters(payload));

        navigate({
            pathname: transaction,
            search: createSearchParams(testFilters).toString(),
        });
    };

    const handleCheckboxChange = (index) => {
        setCheckboxData(checkboxData.map((v, i) => (i === index ? !v : v)));
    };

    const resetSearchProcess = () => {
        setDisableBtn(true);
        setAutocompleteVal("");
        setNoDataMsg(false);
        if (filteredProperties.length > 0) dispatch(setFilteredProperties([]));
    };

    return (
        <Box sx={widgetWrapperStyle}>
            {/* Tabs */}
            <TabContext value={tabValue}>
                <Box
                    sx={{
                        bgcolor: "transparent",
                        py: 2,
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10,
                    }}
                >
                    <TabList
                        onChange={handleTabsChange}
                        aria-label="lab API tabs example"
                        centered
                        TabIndicatorProps={{
                            style: { background: "transparent" },
                        }}
                    >
                        {tabItems.map((tab) => {
                            return (
                                <Tab
                                    key={tab.value}
                                    label={tab.label}
                                    value={tab.value}
                                    data-type={tab.dataType}
                                    sx={{
                                        borderRadius: 50,
                                        minWidth: { xs: 90, md: 120 },
                                        transition: "all 0.2s ease-in-out",
                                        "&.Mui-selected": {
                                            bgcolor: "primary.main",
                                            color: "common.white",
                                        },
                                    }}
                                />
                            );
                        })}
                    </TabList>
                </Box>
                {/* Tab Panels */}
                {/* <Box sx={{ background: "#fff" }}>
                    <TabPanel
                        value="1"
                        sx={{ paddingLeft: 0, paddingRight: 0 }}
                    >
                        <DisplayPropertiesCount />
                    </TabPanel>
                    <TabPanel
                        value="2"
                        sx={{ paddingLeft: 0, paddingRight: 0 }}
                    >
                        <DisplayPropertiesCount />
                    </TabPanel>
                    <TabPanel value="3">Item Three</TabPanel>
                </Box> */}
            </TabContext>
            <Box
                sx={{
                    bgcolor: "primary.lighter",
                    p: { xs: 2, md: 1 },
                    borderRadius: { xs: 2, md: 5 },
                }}
            >
                <Box
                    sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                        flexDirection: { xs: "column", md: "row" },
                        gap: { xs: 2, md: 1 },
                        px: { xs: 2, md: 0 },
                    }}
                >
                    {/* Popover */}
                    <Fragment>
                        <Button
                            size="large"
                            variant="outlined"
                            endIcon={
                                open ? (
                                    <ArrowDropUpIcon />
                                ) : (
                                    <ArrowDropDownIcon color="disabled" />
                                )
                            }
                            ref={anchorRef}
                            onClick={openPopover}
                            sx={{
                                py: 3.4,
                                minWidth: { xs: 300, sm: 380, md: 150 },
                            }}
                        >
                            Type de bien
                        </Button>

                        <MenuPopover
                            open={open}
                            onClose={closePopover}
                            anchorEl={anchorRef.current}
                            anchorOrigin={{
                                vertical: "bottom",
                                horizontal: "left",
                            }}
                            transformOrigin={{
                                vertical: "top",
                                horizontal: "left",
                            }}
                            sx={{ width: 380, my: 1.5, px: 2.5, pt: 3, pb: 1 }}
                        >
                            <Box>
                                <Typography
                                    variant="subtitle1"
                                    noWrap
                                    fontWeight={"bold"}
                                >
                                    Quel type de bien ?
                                </Typography>
                            </Box>

                            <Box sx={{ py: 2, display: "flex", gap: 1 }}>
                                {checkBoxList.map((checkbox, i) => {
                                    return (
                                        <FormControlLabel
                                            key={i}
                                            label={checkbox.name}
                                            control={
                                                <Checkbox
                                                    size="small"
                                                    checked={checkboxData[i]}
                                                    onChange={() => {
                                                        handleCheckboxChange(i);
                                                    }}
                                                />
                                            }
                                        />
                                    );
                                })}
                            </Box>
                        </MenuPopover>
                    </Fragment>
                    {/* Autocomplete */}
                    <Autocomplete
                        sx={{ width: { xs: 300, sm: 380, md: 350 } }}
                        filterOptions={(x) => x}
                        onClose={() => setDepartements([])}
                        noOptionsText={"Saisisser la ville de vos rêves"}
                        onChange={(e, val) => onSearchTermChange(e, val)}
                        value={autocompleteVal}
                        options={
                            departements
                                ? departements.map(
                                      (option) =>
                                          option.properties.postcode +
                                          " - " +
                                          option.properties.city
                                  )
                                : []
                        }
                        renderInput={(params) => (
                            <TextField
                                {...params}
                                label={
                                    transaction === "buy"
                                        ? "Ou voulez-vous acheter"
                                        : "Ou voulez-vous louer"
                                }
                                onChange={(e) => handleAutoCompletion(e)}
                            />
                        )}
                        isOptionEqualToValue={(option, value) =>
                            option.id === value.id
                        } // remove warning: value provided to Autocomplete is invalid
                    />

                    <Button
                        sx={{
                            py: 3.4,
                            minWidth: { xs: 300, sm: 380, md: 200 },
                        }}
                        color={noDataMsg ? "primary" : "secondary"}
                        onClick={onFormSubmit}
                        size="large"
                        variant="contained"
                        startIcon={<SearchIcon />}
                        aria-label="search"
                        disabled={disableBtn}
                    >
                        {filteredProperties.length
                            ? `voir les ${filteredProperties.length} biens`
                            : noDataMsg
                            ? "Aucun bien trouvé"
                            : "Rechercher"}
                    </Button>
                </Box>
            </Box>
        </Box>
    );
}
