import React, { useState, Fragment } from "react";
import { useSelector, useDispatch } from "react-redux";
import AppBar from "@mui/material/AppBar";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import TuneIcon from "@mui/icons-material/Tune";
import PriceFilterPopover from "./PriceFilterPopover";
import TransactionPopover from "./TransactionPopover";

import {
    cityFilter,
    callFilters,
    updateGeometry,
    updateFilter,
} from "../../../../store/actions/propertiesActions";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import FormatListBulletedIcon from "@mui/icons-material/FormatListBulleted";
import { deptsAutocomplete } from "../../../../api/search";
import { formatCity } from "../../../../helpers/formatCity";
import PropertyTypePopover from "./PropertyTypePopover";
import LocationSearchPopover from "./LocationSearchPopover";
import FilterModal from "./FilterModal";
import { getSelectedPropertyType } from "../../../../helpers/filters";

const typeboxItems = [
    {
        value: 2,
        label: "Maison",
    },
    {
        value: 1,
        label: "Appartement",
    },
    {
        value: 3,
        label: "Terrain",
    },
];

export default function PropertiesFilters({
    setLoading,
    isMix,
    handleMainTabsChange,
}) {
    const dispatch = useDispatch();
    const { filteredProperties } = useSelector((state) => state.dahome);
    const [departments, setDepartments] = useState([]);
    const [selectedGeometry, setSelectedGeometry] = useState({});
    const [openFilterModal, setOpenFilterModal] = useState(false);
    const [checkboxData, setCheckboxData] = useState([true, false, false]);
    const [locPopover, setLocPopover] = useState(false);

    const handleOpenFilterModal = () => setOpenFilterModal(true);

    const handleCloseFilterModal = () => setOpenFilterModal(false);

    const extractCityGeolocation = async (location) => {
        const res = await departments.find((city) => {
            return city.properties.label === location.substring(8);
        });

        res !== undefined &&
            setSelectedGeometry({
                lat: res.geometry.coordinates[1],
                lng: res.geometry.coordinates[0],
            });
        return dispatch(
            updateGeometry({
                lat: res.geometry.coordinates[1],
                lng: res.geometry.coordinates[0],
            })
        );
    };

    const handleCityChange = (e, newCity) => {
        if (newCity === null) return;
        setLoading(true);
        extractCityGeolocation(newCity);
        dispatch(cityFilter(formatCity(newCity)));
        dispatch(callFilters({}));
        handleLocPopover();
    };

    const handleAutoCompletion = async (e) => {
        if (e.target.value.length < 4) return;
        const { data } = await deptsAutocomplete(e.target.value);
        setDepartments(data.features);
    };

    const handlePropertyTypeChange = (index) => {
        setCheckboxData(checkboxData.map((v, i) => (i === index ? !v : v)));
    };

    const validatePropertyTypeFilters = () => {
        dispatch(updateFilter({ type: getSelectedPropertyType(checkboxData) }));
        dispatch(callFilters({}));
    };

    const handleLocPopover = () => {
        setLocPopover(!locPopover);
    };

    return (
        <Fragment>
            <AppBar
                sx={{
                    mt: 9,
                    bgcolor: "common.white",
                    pt: { xs: 0, sm: 2 },
                    pb: 2,
                }}
                elevation={1}
            >
                <Container maxWidth="xl">
                    <Stack
                        direction={"row"}
                        justifyContent="space-between"
                        alignItems="center"
                    >
                        <Box sx={{ width: "25%" }}>
                            <Typography
                                variant="body1"
                                color="primary"
                                fontWeight={700}
                            >
                                {filteredProperties.length
                                    ? filteredProperties.length + " Résultats"
                                    : "Aucun résultat"}
                            </Typography>
                        </Box>

                        <Stack
                            direction={"row"}
                            justifyContent="flex-start"
                            alignItems="center"
                            spacing={1}
                            boxShadow={3}
                            sx={{
                                display: { xs: "none", md: "flex" },
                                p: 1.5,
                                borderRadius: 5,
                                bgcolor: "primary.lighter",
                            }}
                        >
                            <TransactionPopover />
                            <LocationSearchPopover
                                open={locPopover}
                                handleLocPopover={handleLocPopover}
                            >
                                <Typography
                                    variant="body1"
                                    sx={{ mb: 2, fontWeight: 600 }}
                                >
                                    A quel endroit ?
                                </Typography>
                                <Autocomplete
                                    size="small"
                                    sx={{ minWidth: 280 }}
                                    filterOptions={(x) => x}
                                    onClose={() => setDepartments([])}
                                    noOptionsText={
                                        "Veuillez entrer une ville ou un département"
                                    }
                                    onChange={(e, val) =>
                                        handleCityChange(e, val)
                                    }
                                    options={
                                        departments
                                            ? departments.map(
                                                  (option) =>
                                                      option.properties
                                                          .postcode +
                                                      " - " +
                                                      option.properties.city
                                              )
                                            : []
                                    }
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label="Où souhaitez-vous louer?"
                                            onChange={(e) =>
                                                handleAutoCompletion(e)
                                            }
                                        />
                                    )}
                                    isOptionEqualToValue={(option, value) =>
                                        option.id === value.id
                                    } // remove  warning value provided to Autocomplete is invalid
                                />
                            </LocationSearchPopover>
                            <PropertyTypePopover
                                checkboxData={checkboxData}
                                handlePropertyTypeChange={
                                    handlePropertyTypeChange
                                }
                                validatePropertyTypeFilters={
                                    validatePropertyTypeFilters
                                }
                                typeboxItems={typeboxItems}
                            />
                            <PriceFilterPopover />
                        </Stack>

                        <Stack
                            direction={"row"}
                            justifyContent="flex-end"
                            alignItems="center"
                            spacing={1}
                            sx={{ width: { xs: "75%", md: "25%" } }}
                        >
                            <Button
                                onClick={handleMainTabsChange}
                                variant={isMix ? "contained" : "outlined"}
                                startIcon={<FormatListBulletedIcon />}
                                sx={{ display: { xs: "none", md: "flex" } }}
                            >
                                Liste
                            </Button>
                            <Button
                                onClick={handleMainTabsChange}
                                variant={isMix ? "outlined" : "contained"}
                                startIcon={<LocationOnIcon />}
                                sx={{ display: { xs: "none", md: "flex" } }}
                            >
                                Carte
                            </Button>
                            <Button
                                onClick={handleOpenFilterModal}
                                variant="outlined"
                                startIcon={<TuneIcon />}
                            >
                                Filtres
                            </Button>
                        </Stack>
                    </Stack>
                </Container>
            </AppBar>

            <FilterModal
                openFilterModal={openFilterModal}
                handleCloseFilterModal={handleCloseFilterModal}
                checkboxData={checkboxData}
                handlePropertyTypeChange={handlePropertyTypeChange}
                typeboxItems={typeboxItems}
                setLoading={setLoading}
            />
        </Fragment>
    );
}
