import React, { Fragment, useState, useRef } from "react";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import MenuPopover from "../../MenuPopover";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";
import { useDispatch, useSelector } from "react-redux";

export default function PropertyTypePopover({
    checkboxData,
    handlePropertyTypeChange,
    typeboxItems,
}) {
    const anchorRef = useRef(null);
    const [open, setOpen] = useState(false);

    const openPopover = () => {
        setOpen(true);
    };

    const closePopover = () => {
        setOpen(false);
    };

    // const DisplayType = () => {
    //     if (!checkboxData[0] && !checkboxData[1] && !checkboxData[2])
    //         return <Fragment>Type de bien</Fragment>;
    //     return checkboxData.map((item, index) => {
    //         if (checkboxData[index])
    //             return (
    //                 <Fragment key={index}>
    //                     {typeboxItems[index].label + "-"}
    //                 </Fragment>
    //             );
    //     });
    // };

    return (
        <Fragment>
            <Button
                //size="large"
                variant="outlined"
                endIcon={open ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />}
                ref={anchorRef}
                onClick={openPopover}
                //sx={{ minWidth: 120, py: 3.4 }}
            >
                {/* <DisplayType /> */}
                Type de bien
            </Button>

            <MenuPopover
                open={open}
                onClose={closePopover}
                anchorEl={anchorRef.current}
                sx={{ width: 400, my: 1.5, px: 2.5, pt: 3, pb: 1 }}
            >
                <Box>
                    <Typography variant="subtitle1" noWrap fontWeight={"bold"}>
                        Quel type de bien ?
                    </Typography>
                </Box>

                <Box sx={{ py: 3, display: "flex", gap: 0 }}>
                    {typeboxItems.map((item, index) => (
                        <FormControlLabel
                            key={index}
                            control={
                                <Checkbox
                                    size="small"
                                    checked={checkboxData[index]}
                                    onClick={() =>
                                        handlePropertyTypeChange(index)
                                    }
                                />
                            }
                            label={item.label}
                        />
                    ))}
                </Box>
            </MenuPopover>
        </Fragment>
    );
}
