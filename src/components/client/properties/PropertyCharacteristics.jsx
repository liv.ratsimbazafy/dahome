import React from "react";
import { Box, Stack, Typography, Divider, Grid } from "@mui/material";

export default function PropertyCharacteristics({ property }) {
    const otherProperty = [
        {
            title: "Bien en copropriété",
            value: property.properties.bienEnCopro ? "Oui" : "Non",
        },
        {
            title: "Taxe foncière",
            value: property.properties.taxeFonciere,
        },
        {
            title: "Honoraire acquéreur",
            value: property.properties.honoraires,
        },
        {
            title: "Etage",
            value: property.properties.floor
                ? property.properties.floor
                : property.properties.floor === 0
                ? "Non"
                : property.properties.floor,
        },
        {
            title: "Exposition",
            value:
                property.properties.exposure ||
                property.properties["exposure-living-room"],
        },
        {
            title: "Lotissement",
            value: property.properties.lotissement ? "Oui" : "Non",
        },
    ];
    return (
        <>
            <Grid container>
                <Grid item xs={12} md={6}>
                    <Typography variant="h4" marginBottom={3}>
                        Intérieur
                    </Typography>

                    <Box
                        display="flex"
                        alignItems={"baseline"}
                        marginBottom={1}
                    >
                        <Typography variant="body1" sx={{ width: "40%" }}>
                            Chambre RDC
                        </Typography>
                        <Typography variant="body1" fontWeight={700}>
                            {property.properties.chambreRDC ? "Oui" : "Non"}
                        </Typography>
                    </Box>

                    <Box
                        display="flex"
                        justifyContent={"flex-start"}
                        alignItems={"baseline"}
                        marginBottom={1}
                    >
                        <Typography variant="body1" sx={{ width: "40%" }}>
                            Type chauffage
                        </Typography>
                        <Typography variant="body1" fontWeight={700}>
                            {property.properties.heating}
                        </Typography>
                    </Box>

                    <Box
                        display="flex"
                        justifyContent={"flex-start"}
                        alignItems={"baseline"}
                        marginBottom={1}
                    >
                        <Typography variant="body1" sx={{ width: "40%" }}>
                            Méca chauffage
                        </Typography>
                        <Typography variant="body1" fontWeight={700}>
                            {property.properties.heating}
                        </Typography>
                    </Box>
                    <Box
                        display="flex"
                        justifyContent={"flex-start"}
                        alignItems={"baseline"}
                        marginBottom={1}
                    >
                        <Typography variant="body1" sx={{ width: "40%" }}>
                            Mode chauffage
                        </Typography>
                        <Typography variant="body1" fontWeight={700}>
                            {property.properties["heating-mode"]}
                        </Typography>
                    </Box>
                    <Box
                        display="flex"
                        justifyContent={"flex-start"}
                        alignItems={"baseline"}
                        marginBottom={1}
                    >
                        <Typography variant="body1" sx={{ width: "40%" }}>
                            Plain-Pied
                        </Typography>
                        <Typography variant="body1" fontWeight={700}>
                            {property.properties.plainPied ? "Oui" : "Non"}
                        </Typography>
                    </Box>
                    <Box
                        display="flex"
                        justifyContent={"flex-start"}
                        alignItems={"baseline"}
                        marginBottom={1}
                    >
                        <Typography variant="body1" sx={{ width: "40%" }}>
                            Salle de bain
                        </Typography>
                        <Typography variant="body1" fontWeight={700}>
                            {property.properties.sDB || property.properties.sDE}
                        </Typography>
                    </Box>
                    <Box
                        display="flex"
                        justifyContent={"flex-start"}
                        alignItems={"baseline"}
                        marginBottom={1}
                    >
                        <Typography variant="body1" sx={{ width: "40%" }}>
                            Toilette
                        </Typography>
                        <Typography variant="body1" fontWeight={700}>
                            {property.properties.wC}
                        </Typography>
                    </Box>
                </Grid>

                <Grid item xs={12} md={6}>
                    <Typography
                        variant="h4"
                        marginBottom={3}
                        marginTop={{ xs: 2, md: 0 }}
                    >
                        Extérieur
                    </Typography>

                    <Box
                        display="flex"
                        justifyContent={"flex-start"}
                        alignItems={"baseline"}
                        marginBottom={1}
                    >
                        <Typography variant="body1" sx={{ width: "40%" }}>
                            Etat Général
                        </Typography>
                        <Typography variant="body1" fontWeight={700}>
                            {property.properties.etatGeneral}
                        </Typography>
                    </Box>
                    <Box
                        display="flex"
                        justifyContent={"flex-start"}
                        alignItems={"baseline"}
                        marginBottom={1}
                    >
                        <Typography variant="body1" sx={{ width: "40%" }}>
                            Neuf - Ancien
                        </Typography>
                        <Typography variant="body1" fontWeight={700}>
                            {property.properties.neufAncien}
                        </Typography>
                    </Box>
                    <Box
                        display="flex"
                        justifyContent={"flex-start"}
                        alignItems={"baseline"}
                        marginBottom={1}
                    >
                        <Typography variant="body1" sx={{ width: "40%" }}>
                            Standing
                        </Typography>
                        <Typography variant="body1" fontWeight={700}>
                            {property.properties.standing}
                        </Typography>
                    </Box>
                    <Box
                        display="flex"
                        justifyContent={"flex-start"}
                        alignItems={"baseline"}
                        marginBottom={1}
                    >
                        <Typography variant="body1" sx={{ width: "40%" }}>
                            Fenêtre
                        </Typography>
                        <Typography variant="body1" fontWeight={700}>
                            {property.properties.fenetres}
                        </Typography>
                    </Box>
                    <Box
                        display="flex"
                        justifyContent={"flex-start"}
                        alignItems={"baseline"}
                        marginBottom={1}
                    >
                        <Typography variant="body1" sx={{ width: "40%" }}>
                            Volet
                        </Typography>
                        <Typography variant="body1" fontWeight={700}>
                            {property.properties.volets
                                ? property.properties.volets
                                : "-"}
                        </Typography>
                    </Box>
                    <Box
                        display="flex"
                        justifyContent={"flex-start"}
                        alignItems={"baseline"}
                        marginBottom={1}
                    >
                        <Typography variant="body1" sx={{ width: "40%" }}>
                            Jardin
                        </Typography>
                        <Typography variant="body1" fontWeight={700}>
                            {property.properties.garden ? "Oui" : "Non"}
                        </Typography>
                    </Box>
                </Grid>
            </Grid>

            <Divider sx={{ my: 5 }} />

            <Typography variant="h4" marginBottom={3}>
                Autres
            </Typography>
            <Grid container>
                <Grid item xs={12} md={6}>
                    {otherProperty.slice(0, 3).map((item, i) => {
                        return (
                            <Box
                                key={i}
                                display="flex"
                                justifyContent={"flex-start"}
                                alignItems={"baseline"}
                                marginBottom={1}
                            >
                                <Typography
                                    variant="body1"
                                    sx={{ width: "40%" }}
                                >
                                    {item.title}
                                </Typography>
                                <Typography variant="body1" fontWeight={700}>
                                    {item.value}
                                </Typography>
                            </Box>
                        );
                    })}
                </Grid>

                <Grid item xs={12} md={6}>
                    {otherProperty.slice(3, 6).map((item, i) => {
                        return (
                            <Box
                                key={i}
                                display="flex"
                                justifyContent={"flex-start"}
                                alignItems={"baseline"}
                                marginBottom={1}
                            >
                                <Typography
                                    variant="body1"
                                    sx={{ width: "40%" }}
                                >
                                    {item.title}
                                </Typography>
                                <Typography variant="body1" fontWeight={700}>
                                    {item.value}
                                </Typography>
                            </Box>
                        );
                    })}
                </Grid>
            </Grid>

            <Divider sx={{ my: 5 }} />
        </>
    );
}
