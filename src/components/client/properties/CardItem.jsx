import React from "react";
import { Link as RouterLink } from "react-router-dom";

import {
    Grid,
    Card,
    CardMedia,
    CardContent,
    Link,
    Box,
    Typography,
    Stack,
} from "@mui/material";
import Carousel from "react-material-ui-carousel";
import user from "../../../assets/images/user_avatar.png";

export default function CardItem({ properties, onMouseOver, onMouseOut }) {
    return properties.map((property) => {
        return (
            <Grid item xs={12} sm={12} md={6} key={property.id} sx={{ mb: 2 }}>
                <Card
                    onMouseEnter={(e) => onMouseOver(e, property.id)}
                    onMouseLeave={(e) => onMouseOut(e)}
                >
                    {property.thumbnails.length < 1 && (
                        <CardMedia
                            component="img"
                            height="250"
                            image={user}
                            alt="placeholder"
                        />
                    )}

                    <Box sx={{ height: 250, backgroundColor: "#f3f3f3" }}>
                        <Carousel autoPlay={false} indicators={false}>
                            {property.thumbnails.map((photo, index) => {
                                return (
                                    <CardMedia
                                        key={index}
                                        component="img"
                                        height="250"
                                        image={photo}
                                        alt={"property-" + index}
                                    />
                                );
                            })}
                        </Carousel>
                    </Box>
                    <Link
                        component={RouterLink}
                        to={`/buy/${property.id}`}
                        underline="none"
                    >
                        <CardContent sx={{ minHeight: 100 }}>
                            <Stack
                                direction="row"
                                justifyContent="space-between"
                                alignItems="center"
                                spacing={2}
                                marginBottom={2}
                            >
                                <Typography
                                    variant="body2"
                                    color="common.black"
                                >
                                    {property.properties.typeBien.toUpperCase()}{" "}
                                    - {property.properties.room} pièces -{" "}
                                    {property.properties.surface}
                                </Typography>
                                <Typography variant="h6" color="secondary">
                                    € {property.price}
                                </Typography>
                            </Stack>
                            <Typography variant="body2" color="common.black">
                                {property.properties.city}
                            </Typography>
                        </CardContent>
                    </Link>
                </Card>
            </Grid>
        );
    });
}
