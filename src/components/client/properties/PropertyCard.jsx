import React from "react";
import { Link as RouterLink } from "react-router-dom";
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";

import {
    Card,
    CardMedia,
    CardContent,
    Link,
    Box,
    Typography,
    Stack,
} from "@mui/material";
import Carousel from "react-material-ui-carousel";
import user from "../../../assets/images/user_avatar.png";

export default function PropertyCard({
    property,
    onMouseOver,
    onMouseOut,
    handleImageLoad,
}) {
    return (
        <Card
            onMouseEnter={
                onMouseOver ? (e) => onMouseOver(e, property.id) : null
            }
            onMouseLeave={onMouseOut ? (e) => onMouseOut(e) : null}
        >
            {property.thumbnails.length < 1 && (
                <CardMedia
                    component="img"
                    height="250"
                    image={user}
                    alt="placeholder"
                />
            )}

            <Box
                sx={{
                    height: { xs: 250, sm: 250 },
                    backgroundColor: "#f3f3f3",
                }}
            >
                <Carousel
                    autoPlay={false}
                    indicators={false}
                    animation={"slide"}
                    navButtonsProps={{
                        style: {
                            backgroundColor: "#fff",
                            color: "#000",
                        },
                    }}
                    indicatorIconButtonProps={{
                        style: {
                            color: "#F1F4F9",
                            zIndex: 999,
                        },
                    }}
                    activeIndicatorIconButtonProps={{
                        style: {
                            color: "#F0B212",
                            zIndex: 999,
                        },
                    }}
                    indicatorContainerProps={{
                        style: {
                            position: "absolute",
                            bottom: "1rem",
                        },
                    }}
                    sx={{ borderRadius: 2 }}
                >
                    {property.thumbnails.map((photo, index) => {
                        return (
                            <CardMedia
                                key={index}
                                component="img"
                                image={photo}
                                alt={"property-" + index}
                                sx={{ height: { xs: 250, sm: 250 } }}
                                onLoad={handleImageLoad}
                            />
                        );
                    })}
                </Carousel>
            </Box>
            <Link
                component={RouterLink}
                to={`/buy/${property.id}`}
                underline="none"
            >
                <CardContent
                    sx={{ minHeight: 110, bgcolor: "common.white", px: 0 }}
                >
                    <Stack
                        direction="row"
                        justifyContent="space-between"
                        alignItems="flex-start"
                        spacing={2}
                        marginBottom={1}
                    >
                        <Box>
                            <Typography variant="body1" color="grey.700">
                                {property.properties.typeBien + " "}{" "}
                                {property.properties.room + " pièces"}
                            </Typography>
                            <Typography variant="body1" color="grey.700">
                                {Math.round(
                                    parseInt(property.properties.surface)
                                )}{" "}
                                m² . {property.properties.bedroom} chb .{" "}
                                {property.properties.sDB
                                    ? property.properties.sDB
                                    : "1"}{" "}
                                sdb
                            </Typography>
                        </Box>
                        <Typography variant="h6" sx={{ fontWeight: 700 }}>
                            {property.price} €
                        </Typography>
                    </Stack>
                    <Stack direction={"row"} alignItems={"center"}>
                        <LocationOnOutlinedIcon sx={{ color: "grey.700" }} />
                        <Typography
                            variant="caption"
                            color="grey.700"
                            sx={{
                                textTransform: "capitalize",
                                fontWeight: 500,
                            }}
                        >
                            {property.properties.city.toLowerCase()}
                        </Typography>
                    </Stack>
                </CardContent>
            </Link>
        </Card>
    );
}
