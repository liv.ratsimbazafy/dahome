import React from "react";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import CloseIcon from "@mui/icons-material/Close";
import { IconButton, Container, Box } from "@mui/material";

export default function PropertyDetailsModal({
    openPropDetailsModal,
    handleClosePropDetailsModal,
    children,
}) {
    return (
        <div>
            <Dialog
                fullScreen
                open={openPropDetailsModal}
                onClose={handleClosePropDetailsModal}
                PaperProps={{
                    style: {
                        backgroundColor: "#000",
                        position: "relative",
                    },
                }}
            >
                <DialogTitle
                    sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                    }}
                >
                    <IconButton
                        aria-label="close"
                        size="small"
                        onClick={handleClosePropDetailsModal}
                    >
                        <CloseIcon />
                    </IconButton>
                </DialogTitle>
                <Container>
                    <Box
                        sx={{
                            position: "absolute",
                            top: "50%",
                            left: "50%",
                            transform: "translate(-50%, -50%)",
                            width: { xs: "100%", md: "60%" },
                            //height: '100%'
                        }}
                    >
                        {children}
                    </Box>
                </Container>
            </Dialog>
        </div>
    );
}
