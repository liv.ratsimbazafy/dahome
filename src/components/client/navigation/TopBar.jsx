import React, { useState, useEffect } from "react";
import { Link as RouterLink, useLocation } from "react-router-dom";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import {
    Container,
    IconButton,
    Link,
    Slide,
    useScrollTrigger,
} from "@mui/material";
import MenuOpenIcon from "@mui/icons-material/MenuOpen";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import logo from "../../../assets/images/logo.svg";

const linkStyle = {
    position: "relative",
    marginLeft: 5,

    "&:before": {
        content: "''",
        position: "absolute",
        width: "0",
        height: "2px",
        bottom: "-5px",
        left: "50%",
        transform: "translate(-50%,0%)",
        backgroundColor: "primary.main",
        visibility: "hidden",
        transition: "all 0.3s ease-in-out",
    },
    "&:hover:before": {
        visibility: "visible",
        width: "100%",
    },
};

const links = [
    {
        href: "/buy",
        title: "Acheter",
    },
    {
        href: "/rent",
        title: "louer",
    },
    {
        href: "/sell",
        title: "vendre",
    },
    {
        href: "/jobs",
        title: "nous recrutons",
    },
];

const linksWrap = {
    display: { xs: "none", md: "flex" },
};

function HideOnScroll(props) {
    const { children, window } = props;
    // Note that you normally won't need to set the window ref as useScrollTrigger
    // will default to window.
    // This is only being set here because the demo is in an iframe.
    const trigger = useScrollTrigger({
        target: window ? window() : undefined,
    });

    return (
        <Slide appear={false} direction="down" in={!trigger}>
            {children}
        </Slide>
    );
}

export default function TopBar(props) {
    const [appBarBg, setAppBarBg] = useState("transparent");
    const location = useLocation();
    const currentLocation = location.pathname;
    
    const toggleTopbar = () => {
        if (window.scrollY > 600) setAppBarBg("common.white");
        else setAppBarBg("transparent");
    };

    useEffect(() => {
        window.addEventListener("scroll", toggleTopbar);

        return () => {
            window.removeEventListener("scroll", toggleTopbar);
        };
    }, []);

    const NavBar = () => (
        <AppBar
            sx={{
                bgcolor: "common.white",
                //bgcolor: appBarBg, ...(currentLocation === '/' && {bgcolor: 'transparent'}), ...(currentLocation !== '/' && {bgcolor: 'common.white'}),
                py: 1,
            }}
            elevation={0}
        >
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <Link component={RouterLink} to="/" underline="none">
                        <Box
                            component="img"
                            src={logo}
                            alt="website logo"
                            sx={{
                                objectFit: "cover",
                                width: { xs: "80%", sm: "100%" },
                            }}
                        />
                    </Link>

                    <Box sx={{ flexGrow: 1 }} />

                    <Box sx={linksWrap}>
                        {links.map((item, i) => {
                            return (
                                <Link
                                    component={RouterLink}
                                    underline="none"
                                    to={item.href}
                                    color="primary"
                                    sx={linkStyle}
                                    key={i}
                                    aria-label={item.title}
                                >
                                    <Typography
                                        component="span"
                                        sx={{
                                            textTransform: "capitalize",
                                        }}
                                    >
                                        {item.title}
                                    </Typography>
                                </Link>
                            );
                        })}
                    </Box>

                    <Link
                        component={RouterLink}
                        to="/login"
                        underline="none"
                        sx={{
                            marginLeft: 5,
                            color: "text.primary",
                            display: { xs: "none", md: "flex" },
                        }}
                        aria-label="link to login page"
                    >
                        <Box sx={{ display: "flex" }}>
                            <AccountCircleIcon />
                            <Typography sx={{ ml: 1 }} variant="body1">
                                Mon compte
                            </Typography>
                        </Box>
                    </Link>
                    <IconButton
                        size="large"
                        edge="end"
                        aria-label="menu"
                        sx={{
                            display: { xs: "flex", md: "none" },
                            mr: 1,
                            bgcolor: "common.white",
                        }}
                        color="primary"
                    >
                        <MenuOpenIcon />
                    </IconButton>
                </Toolbar>
            </Container>
        </AppBar>
    );

    if (currentLocation !== "/") {
        return (
            <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "flex" } }}>
                <NavBar />
            </Box>
        );
    }

    return (
        <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
            <HideOnScroll {...props}>
                <AppBar
                    sx={{
                        bgcolor: appBarBg,
                        //bgcolor: appBarBg, ...(currentLocation === '/' && {bgcolor: 'transparent'}), ...(currentLocation !== '/' && {bgcolor: 'common.white'}),
                        py: 1,
                    }}
                    elevation={0}
                >
                    <Container maxWidth="xl">
                        <Toolbar disableGutters>
                            <Link
                                component={RouterLink}
                                to="/"
                                underline="none"
                                //sx={{ display: { xs: "none", md: "block" } }}
                            >
                                <img
                                    src={logo}
                                    alt="website logo"
                                    sx={{ objectFit: "cover" }}
                                />
                            </Link>

                            <Box sx={{ flexGrow: 1 }} />

                            <Box sx={linksWrap}>
                                {links.map((item, i) => {
                                    return (
                                        <Link
                                            component={RouterLink}
                                            underline="none"
                                            to={item.href}
                                            color="primary"
                                            sx={linkStyle}
                                            key={i}
                                            aria-label={item.title}
                                        >
                                            <Typography
                                                component="span"
                                                sx={{
                                                    textTransform: "capitalize",
                                                }}
                                            >
                                                {item.title}
                                            </Typography>
                                        </Link>
                                    );
                                })}
                            </Box>

                            <Link
                                component={RouterLink}
                                to="/login"
                                underline="none"
                                sx={{
                                    marginLeft: 5,
                                    color: "text.primary",
                                    display: { xs: "none", md: "flex" },
                                }}
                                aria-label="link to login page"
                            >
                                <Box sx={{ display: "flex" }}>
                                    <AccountCircleIcon />
                                    <Typography sx={{ ml: 1 }} variant="body1">
                                        Mon compte
                                    </Typography>
                                </Box>
                            </Link>
                            <IconButton
                                size="large"
                                edge="end"
                                aria-label="menu"
                                sx={{
                                    display: { xs: "flex", md: "none" },
                                    mr: 1,
                                    bgcolor: "common.white",
                                }}
                                color="primary"
                            >
                                <MenuOpenIcon />
                            </IconButton>
                        </Toolbar>
                    </Container>
                </AppBar>
            </HideOnScroll>
        </Box>
    );
}
